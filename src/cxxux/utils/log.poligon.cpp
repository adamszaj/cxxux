/*
 * Copyright (c) 2014, 2015 Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */




#if 0
        class file_output_factory : public  log_output_factory {
            public:
                file_output_factory(const std::string& __path)
                    : log_output_factory{"file"}, path{__path} {}

                /// param uri: file://<path>
                std::unique_ptr<log_output> create(const std::string& uri)
                {
                    return {};
                }
            private:
                std::string path;
        };

        class syslog_output_factory : public log_output_factory {
            public:
                syslog_output_factory(std::string id, int opt, int fac)
                    : log_output_factory{"syslog"}, ident{std::move(id)}, options{opt}, facility{fac}
                {}

                // param uri: syslog://<progname>?options=CONS,PID,PERROR&facility=USER
                std::unique_ptr<log_output> create(const std::string& uri)
                {
                    parse_uri(uri);
                    return std::unique_ptr<log_output>{new syslog_output(ident.c_str(), options, facility)};
                }
            private:
                void parse_uri(const std::string& uri){ /* set ident, options, facility */ }; // TODO

                std::string ident;
                int         options;
                int         facility;
        };
#endif
