/*
 * Copyright (c) 2014, 2015 Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */



#include "weak_singleton.h"
#include <iostream>
#include <vector>

using namespace cxxux::utils;
using namespace std;

class S2 : public weak_singleton<S2> {
    private:
    public:
        S2(){
            std::cout << __PRETTY_FUNCTION__ << std::endl;
        }
        int data;

        ~S2();
        void f() { std::cout << "S2::f()" << std::endl; data  = 32; }
};

class S : public weak_singleton<S> {
    private:
    public:
        S(){ std::cout << __PRETTY_FUNCTION__ << std::endl; }
        int data;
        ~S();
        void f() { std::cout << "S::f()" << std::endl;  data  = 16; }
};

S2::~S2() {
    std::cout << __PRETTY_FUNCTION__ << std::endl;
    try {
        S::get_instance()->f();
    } catch (const singleton_already_deleted& e) {
        std::cout << __PRETTY_FUNCTION__ << " <exc> "<< std::endl;
    }
}

S::~S() {
    std::cout << __PRETTY_FUNCTION__ << std::endl;
    try {
        S2::get_instance()->f();
    } catch (const singleton_already_deleted& e) {
        std::cout << __PRETTY_FUNCTION__ << " <exc> "<< std::endl;
    }
}

int main(int argc, const char *argv[]) try {
    std::vector<std::string> args{argv, argv+argc};
    weak_singleton_holder<S> hs;
    weak_singleton_holder<S2> hs2;

    S2::get_instance();


    return 0;
} catch (singleton_already_deleted& e) {
    cout << "singleton error\n";
}
/* end main() */

