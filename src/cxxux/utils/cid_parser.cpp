/*
 * Copyright (c) 2014, 2015 Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */



#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <string>
#include <map>
#include <map>
#include <syslog.h>

struct cid {
    std::string name;
    cid(std::string s = {}): name(std::move(s)) {}
};

std::ostream& operator << (std::ostream& out, const cid& c) {
    return out << c.name;
}
bool operator < (const cid& cl, const cid& cr) {
    return cl.name < cr.name;
}
bool is_valid_first(char ch) {
    return ch == '_' || std::isalpha(ch);
}
bool is_valid_next(char ch) {
    return ch == '_' || std::isalnum(ch);
}

template<class T>
struct ios_unsetf {

    T& stream;
    std::ios_base::fmtflags fl;

    ios_unsetf(T& s, std::ios_base::fmtflags __fl)
        : stream(s), fl(__fl)
    { stream.unsetf(fl); }

    ~ios_unsetf()
    { stream.setf(fl); }
};


std::istream& operator >> (std::istream& in, cid& c) {
    char ch;
    if (in >> ch) {
        if (is_valid_first(ch)) {
            c.name.clear();
            c.name += ch;
        } else {
            in.unget();
            in.clear(std::ios_base::failbit);
            return in;
        }
        ios_unsetf<std::istream> uf(in, std::ios_base::skipws);
        while (in >> ch) {
            if (is_valid_next(ch)) {
                c.name += ch;
            } else {
                in.unget();
                return in;
            }
        }
    }
    return in;
}
template<class Key, class Value>
void parse_uri_query(std::istream& sin, std::multimap<Key, Value>& mm, char SEP = ',') {
    while (sin) {
        Key key;
        char eq, sep;
        if (sin >> key >> eq && eq == '=') {
            Value value;
            while(sin) {
                sin >> value;
                if(!sin.eof() && (sin.fail() || sin.bad()))
                    break;

                mm.insert(std::make_pair(key, value));
                if (!(sin >> sep) || (sep != ',')) {
                    break;
                }
            }
            if (sin && (sep != '&'))
                break;
        }
    }
}

using namespace std;
int main(int argc, const char *argv[]) {
    std::vector<std::string> args{argv, argv+argc};

    string def{"option=LOG_PID,LOG_PERROR,LOG_CONS&facility=LOG_USER"};
    std::istringstream sin{(args.size() > 1) ? args[1] : def};

    std::multimap<cid, cid> mm;
    std::map<string, int>  syslog_defines {
        // option
        {"option.LOG_CONS",        LOG_CONS},
        {"option.LOG_NDELAY",      LOG_NDELAY},
        {"option.LOG_NOWAIT",      LOG_NOWAIT},
        {"option.LOG_ODELAY",      LOG_ODELAY},
        {"option.LOG_PERROR",      LOG_PERROR},
        {"option.LOG_PID",         LOG_PID},

        // facility
        {"facility.LOG_AUTH",        LOG_AUTH},
        {"facility.LOG_AUTHPRIV",    LOG_AUTHPRIV},
        {"facility.LOG_CRON",        LOG_CRON},
        {"facility.LOG_DAEMON",      LOG_DAEMON},
        {"facility.LOG_FTP",         LOG_FTP},
        {"facility.LOG_KERN",        LOG_KERN},
        {"facility.LOG_LOCAL0",      LOG_LOCAL0},
        {"facility.LOG_LOCAL1",      LOG_LOCAL1},
        {"facility.LOG_LOCAL2",      LOG_LOCAL2},
        {"facility.LOG_LOCAL3",      LOG_LOCAL3},
        {"facility.LOG_LOCAL4",      LOG_LOCAL4},
        {"facility.LOG_LOCAL5",      LOG_LOCAL5},
        {"facility.LOG_LOCAL6",      LOG_LOCAL6},
        {"facility.LOG_LOCAL7",      LOG_LOCAL7},
        {"facility.LOG_LPR",         LOG_LPR},
        {"facility.LOG_MAIL",        LOG_MAIL},
        {"facility.LOG_NEWS",        LOG_NEWS},
        {"facility.LOG_SYSLOG",      LOG_SYSLOG},
        {"facility.LOG_USER",        LOG_USER},
        {"facility.LOG_UUCP",        LOG_UUCP},

        // level
        {"level.LOG_EMERG",       LOG_EMERG},
        {"level.LOG_ALERT",       LOG_ALERT},
        {"level.LOG_CRIT",        LOG_CRIT},
        {"level.LOG_ERR",         LOG_ERR},
        {"level.LOG_WARNING",     LOG_WARNING},
        {"level.LOG_INFO",        LOG_INFO},
        {"level.LOG_NOTICE",      LOG_NOTICE},
        {"level.LOG_DEBUG",       LOG_DEBUG}
    };
    parse_uri_query(sin, mm);


    for (const auto& p: mm) {
        cout << p.first << " => " << p.second << "\n";
        auto it = syslog_defines.find(p.first.name + "." + p.second.name);
        if (it != syslog_defines.end()) {
            cout << "\tvalue: " << it->second << "\n";
        } else {
            cout << "can't find value of '" << p.second.name << "' for '" << p.first << "'\n";
        }
    }

    return 0;
} /* end main() */


