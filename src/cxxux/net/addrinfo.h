#pragma once

#include "socket_address.h"
#include <netdb.h>

namespace cxxux {
    namespace net {
        std::vector<socket_address> resolv_address(std::string name, int pf_family, int sock_type, int ai_flags);
    } /* namespace net */
} /* namespace cxxux */


