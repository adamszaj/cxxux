/*
 * Copyright (c) 2014, 2015 Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */



#include "socket_address.h"
#include "socket.h"

using namespace cxxux::net;
using std::vector;
using std::string;
using std::cout;
using std::cerr;

int main(int argc, char *argv[]) try {

    vector<string> addresses {
        "192.168.0.1",
            "192.168.0.1:8080",
            "[::]",
            "[::]:8080",
            "[::1]",
            "[::1]:8080",
            "[fe80::21f:3bff:fe3d:d52b]",
            "[2001:db8:1:4:ae17:2ff:fe1b:d7a2]",
            "[fe80::21f:3bff:fe3d:d52b]:5454",
            "/root/test/xxx",
            "/var/run/socket",
            "2131.123123.1231"
    };

    for (socket_address s: addresses) {
        cout << s << "\n";
    }

} catch (const std::exception& e) {
    std::cerr << "exc: " << e.what() << "\n";
    return 1;
} catch (...) {
    std::cerr << "unknown exception\n";
    return 2;
}
