/*
 * Copyright (c) 2014, 2015 Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */



#pragma once

#include <cxxux/sys/descriptor.h>
#include <cxxux/sys/file.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "socket_address.h"

namespace cxxux {
    namespace net {

        using cxxux::sys::unique_descriptor;
        using cxxux::sys::shared_descriptor;

        namespace sys = cxxux::sys;

        template<class errhnd>
        unique_descriptor make_socket_eh(int domain, int type, int protocol)
        {
            return unique_descriptor{sys::sys_call<errhnd>(::socket, domain, type, protocol)};
        }

        unique_descriptor make_socket(int domain, int type, int protocol)
        {
            return make_socket_eh<sys::system_error_handler>(domain, type, protocol);
        }

        template<class errhnd>
        int connect_eh(int fd, const socket_address& sa)
        {
            return sys::sys_call<errhnd>(::connect, fd, sa.get<::sockaddr>(), sa.length());
        }

        void connect(int fd, const socket_address& sa)
        {
            connect_eh<sys::system_error_handler>(fd, sa);
        }

        template<class errhnd>
        unique_descriptor connect_eh(const socket_address& sa, int type, int protocol)
        {
            auto fd = make_socket_eh<errhnd>(sa.family(), type, protocol);
            connect_eh<errhnd>(fd.get(), sa);
            return fd;
        }

        unique_descriptor connect(const socket_address& sa, int type, int protocol)
        {
            return connect_eh<sys::system_error_handler>(sa, type, protocol);
        }

        template<class errhnd>
        void listen_eh(int fd, int backlog)
        {
            sys::sys_call<errhnd>(::listen, fd, backlog);
        }

        void listen(int fd, int backlog)
        {
            listen_eh<sys::system_error_handler>(fd, backlog);
        }

        template<class errhnd>
        int bind_eh(int fd, const socket_address& sa)
        {
            return sys::sys_call<errhnd>(::bind, fd, sa.get<sockaddr>(), sa.length());
        }

        void bind(int fd, const socket_address& sa)
        {
            bind_eh<sys::system_error_handler>(fd, sa);
        }

        template<class errhnd>
        unique_descriptor bind_eh(const socket_address& sa, int type, int protocol = {})
        {
            auto fd = make_socket_eh<errhnd>(sa.family(), type, protocol);
            bind_eh<errhnd>(fd.get(), sa);
            return fd;
        }

        unique_descriptor bind(const socket_address& sa, int type, int protocol = {})
        {
            return bind_eh<sys::system_error_handler>(sa, type, protocol);
        }

        template<class errhnd>
        unique_descriptor accept_eh(int fd, socket_address& sa)
        {
            auto len = sa.length();
            return unique_descriptor{sys::sys_call<errhnd>(::accept, fd, sa.get<sockaddr>(), &len)};
        }

        static inline
        unique_descriptor accept(int fd, socket_address& sa) {
            return accept_eh<sys::system_error_handler>(fd, sa);
        }

        template<class errhnd>
        unique_descriptor accept_eh(int fd) {
            return unique_descriptor{sys::sys_call<errhnd>(::accept, fd, nullptr, nullptr)};
        }

        static inline
        unique_descriptor accept(int fd) {
            return accept_eh<sys::system_error_handler>(fd);
        }

        template<class errhnd>
        ssize_t send_eh(int sd, const void* data, size_t size, int flags) {
            return sys::sys_call<errhnd>(::send, sd, data, size, flags);
        }

        static inline
        ssize_t send(int sd, const void* data, size_t size, int flags) {
            return send_eh<sys::system_error_handler>(sd, data, size, flags);
        }

        template<class errhnd>
        ssize_t recv_eh(int sd, void* data, size_t size, int flags) {
            return sys::sys_call<errhnd>(::recv, sd, data, size, flags);
        }

        static inline
        ssize_t recv(int sd, void* data, size_t size, int flags) {
            return recv_eh<sys::system_error_handler>(sd, data, size, flags);
        }

        template<class errhnd>
        ssize_t sendto_eh(int sd, const void* data, size_t size, int flags, const socket_address& sa) {
            return sys::sys_call<errhnd>(::sendto, sd, data, size, flags, sa.get<sockaddr>(), sa.length());
        }

        static inline
        ssize_t sendto(int sd, const void* data, size_t size, int flags, const socket_address& sa) {
            return sendto_eh<sys::system_error_handler>(sd, data, size, flags, sa);
        }

        template<class errhnd>
        ssize_t recvfrom_eh(int sd, void* data, size_t size, int flags, socket_address& sa) {
            auto len = sa.length();
            return sys::sys_call<errhnd>(::recvfrom, sd, data, size, flags, sa.get<sockaddr>(), &len);
        }

        static inline
        ssize_t recvfrom(int sd, void* data, size_t size, int flags, socket_address& sa) {
            return recvfrom_eh<sys::system_error_handler>(sd, data, size, flags, sa);
        }

        template<class errhnd>
        ssize_t sendmsg_eh(int sd, const msghdr* msg, int flags) {
            return sys::sys_call<errhnd>(::sendmsg, sd, msg, flags);
        }

        static inline
        ssize_t sendmsg(int sd, const msghdr* msg, int flags) {
            return sendmsg_eh<sys::system_error_handler>(sd, msg, flags);
        }

        template<class errhnd>
        ssize_t recvmsg_eh(int sd, msghdr* msg, int flags) {
            return sys::sys_call<errhnd>(::recvmsg, sd, msg, flags);
        }

        static inline
        ssize_t recvmsg(int sd, msghdr* msg, int flags) {
            return recvmsg_eh<sys::system_error_handler>(sd, msg, flags);
        }

        template<class errhnd>
        int setsockopt_eh(int sd, int level, int optname, const void *optval, socklen_t optlen) {
            return sys::sys_call<errhnd>(::setsockopt, sd, level, optname, optval, optlen);
        }

        static inline
        int setsockopt(int sd, int level, int optname, const void *optval, socklen_t optlen) {
            return setsockopt_eh<sys::system_error_handler>(sd, level, optname, optval, optlen);
        }

        template<class errhnd>
        int getsockopt_eh(int sd, int level, int optname, void *optval, socklen_t* optlen) {
            return sys::sys_call<errhnd>(::getsockopt, sd, level, optname, optval, optlen);
        }

        static inline
        int getsockopt(int sd, int level, int optname, void *optval, socklen_t* optlen) {
            return getsockopt_eh<sys::system_error_handler>(sd, level, optname, optval, optlen);
        }

        /**
         * shutdown
         */
        template<class errhnd>
        void shutdown_rd_eh(int sd) {
            sys::sys_call<errhnd>(::shutdown, sd, SHUT_RD);
        }

        template<class errhnd>
        void shutdown_wr_eh(int sd) {
            sys::sys_call<errhnd>(::shutdown, sd, SHUT_WR);
        }

        template<class errhnd>
        void shutdown_rdwr_eh(int sd) {
            sys::sys_call<errhnd>(::shutdown, sd, SHUT_RDWR);
        }

        static inline
        void shutdown_rd(int sd) {
            shutdown_rd_eh<sys::system_error_handler>(sd);
        }

        static inline
        void shutdown_wr(int sd) {
            shutdown_wr_eh<sys::system_error_handler>(sd);
        }

        static inline
        void shutdown_rdwr(int sd) {
            shutdown_rdwr_eh<sys::system_error_handler>(sd);
        }

        struct socket_ioflag {
            int value;

            explicit socket_ioflag(int fl)
                : value{fl} {}

            operator int() const {
                return value;
            }
        };

        template<class Desc, class EH = sys::system_error_handler, class AEH = sys::asystem_error_handler>
        class socket : public sys::file<Desc, EH, AEH>
        {
            public:

                using descriptor_type       = Desc;
                using default_error_handler = EH;
                using async_error_handler   = AEH;
                using super = sys::file<descriptor_type, EH, AEH>;

                // use constructors from file
                using sys::file<descriptor_type>::file;

                socket(const socket& ) = default;
                socket(socket&& ) = default;
                socket& operator=(const socket& ) = default;
                socket& operator=(socket&& ) = default;


                explicit socket(int family, int type, int protocol = {})
                    : super{descriptor_type{make_socket_eh<default_error_handler>(family, type, protocol)}}
                {
                }

                template<class errhnd>
                void bind_eh(const socket_address& sa, int type, int protocol = {}) {
                    super::fd(cxxux::net::bind_eh<errhnd>(sa, type, protocol));
                }

                void bind(const socket_address& sa, int type, int protocol = {}) {
                    bind_eh<default_error_handler>(sa, type, protocol);
                }

                template<class errhnd>
                void connect_eh(const socket_address& sa, int type, int protocol = {}) {
                    super::fd(cxxux::net::connect_eh<errhnd>(sa, type, protocol));
                }

                void connect(const socket_address& sa, int type, int protocol = {}) {
                    connect_eh<default_error_handler>(sa, type, protocol);
                }

                template<class errhnd>
                void bind_eh(const socket_address& sa) {
                    cxxux::net::bind_eh<errhnd>(super::fd().get(), sa);
                }

                void bind(const socket_address& sa) {
                    bind_eh<default_error_handler>(sa);
                }

                template<class errhnd>
                void connect_eh(const socket_address& sa) {
                    cxxux::net::connect_eh<errhnd>(super::fd().get(), sa);
                }

                void connect(const socket_address& sa) {
                    connect_eh<default_error_handler>(sa);
                }

                template<class errhnd>
                ssize_t send_eh(const void* data, size_t size, socket_ioflag flags) {
                    return cxxux::net::send_eh<errhnd>(super::fd().get(), data, size, flags);
                }

                ssize_t send(const void* data, size_t size, socket_ioflag flags) {
                    return send_eh<default_error_handler>(data, size, flags);
                }

                template<class errhnd>
                ssize_t recv_eh(void* data, size_t size, socket_ioflag flags) {
                    return cxxux::net::recv_eh<errhnd>(super::fd().get(), data, size, flags);
                }

                ssize_t recv(void* data, size_t size, socket_ioflag flags) {
                    return recv_eh<default_error_handler>(data, size, flags);
                }

                template<class errhnd>
                ssize_t sendto_eh(const void* data, size_t size, socket_ioflag flags, const socket_address& sa) {
                    return cxxux::net::sendto_eh<errhnd>(super::fd().get(), data, size, flags, sa);
                }

                ssize_t sendto(const void* data, size_t size, socket_ioflag flags, const socket_address& sa) {
                    return sendto_eh<default_error_handler>(data, size, flags, sa);
                }

                template<class errhnd>
                ssize_t recvfrom_eh(void* data, size_t size, socket_ioflag flags, socket_address& sa) {
                    return cxxux::net::recvfrom_eh<errhnd>(super::fd().get(), data, size, flags, sa);
                }

                ssize_t recvfrom(void* data, size_t size, socket_ioflag flags, socket_address& sa) {
                    return recvfrom_eh<default_error_handler>(super::fd().get(), data, size, flags, sa);
                }

                template<class errhnd>
                ssize_t sendmsg_eh(const msghdr* msg, socket_ioflag flags) {
                    return cxxux::net::sendmsg_eh<default_error_handler>(super::fd().get(), msg, flags);
                }

                ssize_t sendmsg(const msghdr* msg, socket_ioflag flags) {
                    return sendmsg_eh<default_error_handler>(msg, flags);
                }

                template<class errhnd>
                ssize_t recvmsg_eh(msghdr* msg, socket_ioflag flags) {
                    return cxxux::net::recvmsg_eh<errhnd>(super::fd().get(), msg, flags);
                }

                ssize_t recvmsg(msghdr* msg, socket_ioflag flags) {
                    return recvmsg_eh<default_error_handler>(msg, flags);
                }

                // recv
                //
                template<class errhnd, class T> ssize_t recv_eh(std::vector<T>& v, size_t off, size_t n, socket_ioflag flags) {
                    static_assert(std::is_pod<T>::value, "std::vector::value_type is not a POD type");
                    super::check_range(v.size(), off, n);
                    if (n > 0)
                        return recv_eh<errhnd>(v.data()+off, sizeof(T)*n, flags);
                    return 0;
                }

                template<class errhnd, class T> ssize_t recv_eh(std::vector<T>& v, size_t off, socket_ioflag flags) {
                    return recv_eh<errhnd>(v, off, v.size()-off, flags);
                }

                template<class errhnd, class T> ssize_t recv_eh(std::vector<T>& v, socket_ioflag flags) {
                    return recv_eh<errhnd>(v, 0, v.size(), flags);
                }
                //
                template<class T> ssize_t recv(std::vector<T>& v, size_t off, size_t n, socket_ioflag flags) {
                    return recv_eh<default_error_handler>(v, off, n, flags);
                }

                template<class T>
                ssize_t recv(std::vector<T>& v, size_t off, socket_ioflag flags) {
                    return recv_eh<default_error_handler>(v, off, flags);
                }

                template<class T>
                ssize_t recv(std::vector<T>& v, socket_ioflag flags) {
                    return recv_eh<default_error_handler>(v, flags);
                }

                // send
                template<class errhnd, class T> ssize_t send_eh(const std::vector<T>& v, size_t off, size_t n, socket_ioflag flags) {
                    static_assert(std::is_pod<T>::value, "std::vector::value_type is not a POD type");
                    super::check_range(v.size(), off, n);
                    if (n > 0)
                        return send_eh<errhnd>(v.data()+off, sizeof(T)*n, flags);
                    return 0;
                }

                template<class errhnd, class T> ssize_t send_eh(const std::vector<T>& v, size_t off, socket_ioflag flags) {
                    return send_eh<errhnd>(v, off, v.size()-off, flags);
                }

                template<class errhnd, class T> ssize_t send_eh(const std::vector<T>& v, socket_ioflag flags) {
                    return send_eh<errhnd>(v, 0, v.size(), flags);
                }

                //
                template<class T> ssize_t send(const std::vector<T>& v, size_t off, size_t n, socket_ioflag flags) {
                    return send_eh<default_error_handler>(v, off, n, flags);
                }

                template<class T> ssize_t send(const std::vector<T>& v, size_t off, socket_ioflag flags) {
                    return send_eh<default_error_handler>(v, off, flags);
                }

                template<class T> ssize_t send(const std::vector<T>& v, socket_ioflag flags) {
                    return send_eh<default_error_handler>(v, flags);
                }

                template<class T> ssize_t asend(const std::vector<T>& v, size_t off, size_t n, socket_ioflag flags) {
                    return send_eh<async_error_handler>(v, off, n, flags);
                }

                template<class T> ssize_t asend(const std::vector<T>& v, size_t off, socket_ioflag flags) {
                    return send_eh<async_error_handler>(v, off, flags);
                }

                template<class T> ssize_t asend(const std::vector<T>& v, socket_ioflag flags) {
                    return send_eh<async_error_handler>(v, flags);
                }

                // recvfrom
                template<class errhnd, class T> ssize_t recvfrom_eh(std::vector<T>& v, size_t off, size_t n, socket_ioflag flags, socket_address& sa) {
                    static_assert(std::is_pod<T>::value, "std::vector::value_type is not a POD type");
                    super::check_range(v.size(), off, n);
                    if (n > 0)
                        return recvfrom_eh<errhnd>(v.data()+off, sizeof(T)*n, flags, sa);
                    return 0;
                }

                template<class errhnd, class T> ssize_t recvfrom_eh(std::vector<T>& v, size_t off, socket_ioflag flags, socket_address& sa) {
                    return recvfrom_eh<errhnd>(v, off, v.size()-off, flags, sa);
                }

                template<class errhnd, class T> ssize_t recvfrom_eh(std::vector<T>& v, socket_ioflag flags, socket_address& sa) {
                    return recvfrom_eh<errhnd>(v, 0, v.size(), flags, sa);
                }

                //
                template<class T> ssize_t recvfrom(std::vector<T>& v, size_t off, size_t n, socket_ioflag flags, socket_address& sa) {
                    return recvfrom_eh<default_error_handler>(v, off, n, flags, sa);
                }

                template<class T> ssize_t recvfrom(std::vector<T>& v, size_t off, socket_ioflag flags, socket_address& sa) {
                    return recvfrom_eh<default_error_handler>(v, off, flags, sa);
                }

                template<class T> ssize_t recvfrom(std::vector<T>& v, socket_ioflag flags, socket_address& sa) {
                    return recvfrom_eh<default_error_handler>(v, flags, sa);
                }

                // sendto
                template<class errhnd, class T>
                ssize_t sendto_eh(const std::vector<T>& v, size_t off, size_t n, socket_ioflag flags, const socket_address& sa) {
                    static_assert(std::is_pod<T>::value, "std::vector::value_type is not a POD type");
                    super::check_range(v.size(), off, n);
                    if (n > 0)
                        return sendto_eh<errhnd>(v.data()+off, sizeof(T)*n, flags, sa);
                    return 0;
                }

                template<class errhnd, class T>
                ssize_t sendto_eh(const std::vector<T>& v, size_t off, socket_ioflag flags, const socket_address& sa) {
                    return sendto_eh<errhnd>(v, off, v.size()-off, flags, sa);
                }

                template<class errhnd, class T>
                ssize_t sendto_eh(const std::vector<T>& v, socket_ioflag flags, const socket_address& sa) {
                    return sendto_eh<errhnd>(v, 0, v.size(), flags, sa);
                }

                template<class T>
                ssize_t sendto(const std::vector<T>& v, size_t off, size_t n, socket_ioflag flags, const socket_address& sa) {
                    return sendto_eh<default_error_handler>(v, off, n, flags, sa);
                }

                template<class T>
                ssize_t sendto(const std::vector<T>& v, size_t off, socket_ioflag flags, const socket_address& sa) {
                    return sendto_eh<default_error_handler>(v, off, flags, sa);
                }

                template<class T>
                ssize_t sendto(const std::vector<T>& v, socket_ioflag flags, const socket_address& sa) {
                    return sendto_eh<default_error_handler>(v, flags, sa);
                }

                // set/get sockopt
                template<class errhnd, class Opt>
                int setsockopt_eh(int level, int optname, Opt optval) {
                    socklen_t optlen = sizeof(Opt);
                    return cxxux::net::setsockopt_eh<errhnd>(super::fd().get(), level, optname, &optval, optlen);
                }

                template<class errhnd, class Opt>
                int getsockopt_eh(int level, int optname, Opt& optval) {
                    socklen_t optlen = sizeof(Opt);
                    return cxxux::net::getsockopt_eh<errhnd>(super::fd().get(), level, optname, &optval, &optlen);
                }

                template<class Opt>
                int setsockopt(int level, int optname, Opt optval) {
                    return setsockopt_eh<default_error_handler>(level, optname, optval);
                }

                template<class Opt>
                int getsockopt(int level, int optname, Opt& optval) {
                    return getsockopt_eh<default_error_handler>(level, optname, optval);
                }

                // shutdown socket
                template<class errhnd>
                void shutdown_rd_eh() {
                    cxxux::net::shutdown_rd_eh<errhnd>(super::fd().get());
                }

                template<class errhnd>
                void shutdown_wr_eh() {
                    cxxux::net::shutdown_wr_eh<errhnd>(super::fd().get());
                }

                template<class errhnd>
                void shutdown_rdwr_eh() {
                    cxxux::net::shutdown_rdwr_eh<errhnd>(super::fd().get());
                }

                void shutdown_rd() {
                    shutdown_rd_eh<default_error_handler>();
                }

                void shutdown_wr() {
                    shutdown_wr_eh<default_error_handler>();
                }

                void shutdown_rdwr() {
                    shutdown_rdwr_eh<default_error_handler>();
                }

        };

        template<class Desc> class stream_socket : public socket<Desc> {
            public:
                using descriptor_type = Desc;
                using socket<descriptor_type>::socket;

                explicit stream_socket(int af, int protocol = {})
                    : socket<descriptor_type>{af, SOCK_STREAM, protocol}
                {}

                explicit stream_socket(const socket_address& sa, int protocol = {})
                    : stream_socket{descriptor_type{cxxux::net::connect(sa, SOCK_STREAM, protocol)}}
                {}

        };

        template<class Desc> class datagram_socket : public socket<Desc> {
            public:
                using descriptor_type = Desc;
                using socket<descriptor_type>::socket;

                explicit datagram_socket(int af, int protocol = {})
                    : socket<descriptor_type>{af, SOCK_DGRAM, protocol}
                {}

                explicit datagram_socket(const socket_address& sa, int protocol = {})
                    : datagram_socket{descriptor_type{cxxux::net::bind(sa, SOCK_DGRAM, protocol)}}
                {}
        };

        class server_socket {
            public:

                explicit server_socket(const socket_address& sa, int backlog)
                    : desc{cxxux::net::bind(sa, SOCK_STREAM)}
                {
                    cxxux::net::listen(desc.get(), backlog);
                }

                server_socket(const server_socket&) = delete;

                server_socket(server_socket&& ss)
                    : desc(std::move(ss.desc))
                {}

                server_socket()
                {}

                void bind(const socket_address& sa, int backlog) {
                    desc = cxxux::net::bind(sa, SOCK_STREAM);
                    cxxux::net::listen(desc.get(), backlog);
                }

                unique_descriptor accept(socket_address& sa) {
                    return cxxux::net::accept(desc.get(), sa);
                }

                unique_descriptor accept() {
                    return cxxux::net::accept(desc.get());
                }

                const unique_descriptor& fd() const {
                    return desc;
                }
            private:
                unique_descriptor desc;
        };

    } /* namespace net */
} /* namespace cxxux */
