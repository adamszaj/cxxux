
#include "addrinfo.h"
#include <vector>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#include <cxxux/sys/system.h>

namespace cxxux {
    namespace net {

        void handle_ai_error(int errcode, std::string name) {
            if (errcode == EAI_SYSTEM) {
                sys_throw(std::system_error(sys_error_code(cxxux_errno), "Resolving: " + name + " failed"));
            } else {
                sys_throw(std::logic_error(gai_strerror(errcode)));
            }
        }

        struct addrinfo_deleter {
            void operator()(::addrinfo* ai) {
                if (ai) ::freeaddrinfo(ai);
            }
        };

        std::vector<socket_address> resolv_address(std::string name, int pf_family, int sock_type, int ai_flags) {
            ::addrinfo *res = nullptr;
            ::addrinfo hints;
            int errcode;
            memset (&hints, 0, sizeof (hints));

            hints.ai_family     = pf_family;
            hints.ai_socktype   = sock_type;
            hints.ai_flags      = ai_flags;

            errcode = ::getaddrinfo (name.c_str(), NULL, &hints, &res);

            std::unique_ptr<::addrinfo, addrinfo_deleter> ai{res};

            if (errcode != 0) {
                handle_ai_error(errcode, name);
            }

            std::vector<socket_address> addresses;
            for (::addrinfo *info = res; info; info = info->ai_next) {
                addresses.push_back(socket_address(info->ai_addr, info->ai_addrlen));
            }
            return addresses;
        }


    } /* namespace net */
} /* namespace cxxux */


