/*
 * Copyright (c) 2014, 2015 Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */



#include "socket_address.h"

#include <arpa/inet.h>
#include <stdexcept>

#ifdef USE_STD_REGEX
#include <regex>
#else
#include <boost/regex.hpp>
#endif

#ifdef unix
#undef unix
#endif

namespace cxxux {
    namespace net {

#ifdef USE_STD_REGEX
        namespace rex_ns = std;
#else
        namespace rex_ns = boost;
#endif

        namespace {
            /// TODO move to "c++ lang utils"
            /// implementation from: TC++PL B.~Stroustrup
            template<class T, class S>
                T narrow_cast(S s) {
                    auto r = static_cast<T>(s);
                    if (static_cast<S>(r) != s)
                        throw std::runtime_error{"narrow_cast<>() failed"};
                    return r;
                }

            enum class address_type {
                none,
                inet,
                inet_port,
                inet6,
                inet6_port,
                unix
            };

            address_type match_addr(rex_ns::smatch& match, const std::string& saddr)
            {
                static const std::string inet_addr_rex   {"([[:digit:]]+)(\\.[[:digit:]]+){3}"};
                static const std::string port_rex        {"[[:digit:]]+"};
                static const std::string inet6_seg       {"[[:xdigit:]]{0,4}"};
                static const std::string inet6_addr_rex  {inet6_seg + "(:" + inet6_seg + "){1,7}(:" + inet_addr_rex + ")?"};
                static const std::string unix_rex        {"(/[^\\/]+)+"};

                static const rex_ns::regex re_inet_addr        {"(" + inet_addr_rex + ")"};
                static const rex_ns::regex re_inet_addr_port   {"(" + inet_addr_rex + "):(" + port_rex + ")"};

                static const rex_ns::regex re_inet6_addr       {"\\[(" + inet6_addr_rex + ")\\]"};
                static const rex_ns::regex re_inet6_addr_port  {"\\[(" + inet6_addr_rex + ")\\]:(" + port_rex + ")"};

                static const rex_ns::regex re_unix_addr        {unix_rex};

                if (rex_ns::regex_match(saddr, match, re_inet_addr)) {
                    return address_type::inet;
                } else if (rex_ns::regex_match(saddr, match, re_inet_addr_port)) {
                    return address_type::inet_port;
                } else if (rex_ns::regex_match(saddr, match, re_inet6_addr)) {
                    return address_type::inet6;
                } else if (rex_ns::regex_match(saddr, match, re_inet6_addr_port)) {
                    return address_type::inet6_port;
                } else if (rex_ns::regex_match(saddr, match, re_unix_addr)) {
                    return address_type::unix;
                }

                return address_type::none;
            }

            void set_sin(::sockaddr_in* sin_saddr, socklen_t len, const std::string& saddr, const std::string& a, unsigned short port) {
                if (sizeof(::sockaddr_in) <= len) {
                    sin_saddr->sin_family = AF_INET;

                    if(!inet_pton(AF_INET, a.c_str(), &sin_saddr->sin_addr))
                        throw std::runtime_error{"Can't convert '" + saddr +"' to inet address"};

                    sin_saddr->sin_port = htons(port);
                } else {
                    throw std::length_error{"Can't convert '" + saddr +"' to inet address: dest size is too small"};
                }
            }


            void set_sin6(::sockaddr_in6* sin6_saddr, socklen_t len, const std::string& saddr, const std::string& a, unsigned short port) {
                if (sizeof(::sockaddr_in6) <= len) {
                    sin6_saddr->sin6_family = AF_INET6;

                    if(!inet_pton(AF_INET6, a.c_str(), &sin6_saddr->sin6_addr))
                        throw std::runtime_error{"Can't convert '" + saddr +"' to inet6 address"};

                    sin6_saddr->sin6_port = htons(port);
                } else {
                    throw std::length_error{"Can't convert '" + saddr +"' to inet6 address: dest size is too small"};
                }
            }

            void set_un(::sockaddr_un* sun_saddr, socklen_t len, const std::string& saddr, const std::string& p) {
                if (sizeof(::sockaddr_un) <= len) {
                    sun_saddr->sun_family = AF_UNIX;
                    if (p.size() < sizeof(sun_saddr->sun_path)) {
                        strncpy(sun_saddr->sun_path, p.c_str(), sizeof(sun_saddr->sun_path));
                    } else {
                        throw std::length_error{"Path is too long"};
                    }
                } else {
                    throw std::length_error{"Can't convert '" + saddr +"' to inet6 address: dest size is too small"};
                }
            }
        }

        int __make_sockaddr(sockaddr* addr, socklen_t len, const std::string& saddr, unsigned short default_port) {
            rex_ns::smatch match;
            auto type = match_addr(match, saddr);
            switch (type) {
                case address_type::inet:
                    set_sin(reinterpret_cast<::sockaddr_in*>(addr), len, saddr, match.str(1), default_port);
                    break;
                case address_type::inet_port:
                    set_sin(reinterpret_cast<::sockaddr_in*>(addr), len, saddr, match.str(1), narrow_cast<unsigned short>(stoul(match.str(4))));
                    break;
                case address_type::inet6:
                    set_sin6(reinterpret_cast<::sockaddr_in6*>(addr), len, saddr, match.str(1), default_port);
                    break;
                case address_type::inet6_port:
                    set_sin6(reinterpret_cast<::sockaddr_in6*>(addr), len, saddr, match.str(1), narrow_cast<unsigned short>(stoul(match.str(6))));
                    break;
                case address_type::unix:
                    set_un(reinterpret_cast<::sockaddr_un*>(addr), len, saddr, match.str(0));
                    break;
                case address_type::none:
                    throw std::runtime_error{"Given string: '" + saddr + "' can't be converted to socket address"};
                    break;
            }
            return 0;
        }

        std::ostream& socket_address::print(std::ostream& out) const {
            std::vector<char> str(100);
            switch (family()) {
                case AF_INET:
                    {
                        if (inet_ntop(AF_INET, &get<sockaddr_in>()->sin_addr, str.data(), str.size())) {
                            out << str.data();
                            auto p = ntohs(get<sockaddr_in>()->sin_port);
                            if (p) // print port only if not equal to 0
                                out << ':' << p;
                        }
                    }
                    break;
                case AF_INET6:
                    {
                        if (inet_ntop(AF_INET6, &get<sockaddr_in6>()->sin6_addr, str.data(), str.size())) {
                            out << "[" << str.data() << "]";
                            auto p = ntohs(get<sockaddr_in6>()->sin6_port);
                            if (p) // print port only if not equal to 0
                                out << ':' << p;
                        }
                    }
                    break;
                case AF_UNIX:
                    out << get<sockaddr_un>()->sun_path;
                    break;
                default:
                    break;
            }
            return out;
        }

    } /* namespace net */
} /* namespace cxxux */

