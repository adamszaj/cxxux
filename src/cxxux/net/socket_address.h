/*
 * Copyright (c) 2014, 2015 Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */



#pragma once

#include <string>
#include <cstdint>
#include <cstring>
#include <sys/socket.h>
#include <netinet/in.h>
#include <linux/un.h>

#include "cxxux/sys/file.h"

namespace cxxux {
    namespace net {
        template<class T> struct is_addr_supported {
            static constexpr bool value{false};
        };
        template<> struct is_addr_supported<::sockaddr> {
            static constexpr bool value{true};
        };
        template<> struct is_addr_supported<::sockaddr_in> {
            static constexpr bool value{true};
        };
        template<> struct is_addr_supported<::sockaddr_in6> {
            static constexpr bool value{true};
        };
        template<> struct is_addr_supported<::sockaddr_un> {
            static constexpr bool value{true};
        };
        template<> struct is_addr_supported<::sockaddr_storage> {
            static constexpr bool value{true};
        };

        extern int __make_sockaddr(sockaddr* addr, socklen_t len, const std::string& saddr, unsigned short default_port = 0);

        template<class A>
            int make_sockaddr(A& addr, const std::string& saddr, unsigned short default_port = 0) {
                static_assert(cxxux::net::is_addr_supported<A>::value, "Source sockaddr type is not supported");
                return __make_sockaddr(reinterpret_cast<sockaddr*>(&addr), sizeof(A), saddr, default_port);
            }

        class socket_address {
            public:
                socket_address()
                {
                    memset(&addr, 0, sizeof(sockaddr_storage));
                    addr.ss_family = AF_UNSPEC;
                }

                // implicit conversion from string
                socket_address(const std::string& saddr, uint16_t default_port = 0)
                    : socket_address{}
                {
                    make_sockaddr(addr, saddr, default_port);
                }

                socket_address(const socket_address& ) = default;

                template<class A> explicit socket_address(const A& raw_addr)
                    : socket_address{}
                {
                    static_assert(cxxux::net::is_addr_supported<A>::value, "Source sockaddr type is not supported");
                    memcpy(&addr, &raw_addr, sizeof(A));
                }

                explicit socket_address(::sockaddr* raw_addr, socklen_t len)
                    : socket_address{}
                {
                    if (len <= sizeof(addr)) {
                        memcpy(&addr, raw_addr, len);
                    } else {
                        throw std::length_error{"len value is too big"};
                    }
                }

                sa_family_t family() const {
                    return addr.ss_family;
                }

                socklen_t length() const {
                    return sizeof(addr);
                }

                std::ostream& print(std::ostream& out) const;


                // get pointer to the internal struct
                template<class A> A* get() {
                    static_assert(cxxux::net::is_addr_supported<A>::value, "Destination sockaddr type is not supported");
                    return reinterpret_cast<A*>(&addr);
                }

                // get pointer to the internal struct
                template<class A> const A* get() const {
                    static_assert(cxxux::net::is_addr_supported<A>::value, "Destination sockaddr type is not supported");
                    return reinterpret_cast<const A*>(&addr);
                }

            private:
                /* data */
                sockaddr_storage addr;

        }; /* class socket_address */

        static inline
        std::ostream& operator << (std::ostream& out, const socket_address& sa)
        {
            return sa.print(out);
        }

    } /* namespace net */

} /* namespace cxxux */

