/*
 * Copyright (c) 2014, 2015 Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */



#pragma once

#include "sys_call.h"
#include <signal.h>
#include <initializer_list>

namespace cxxux {
    namespace sys {

        class signal_set {
            public:
                struct fill_set{};
                struct empty_set{};

                signal_set()
                {
                    clear();
                }

                signal_set(empty_set e)
                    : signal_set()
                {
                }

                signal_set(std::initializer_list<int> signals)
                    : signal_set()
                {
                    add(signals);
                }

                signal_set(empty_set e, std::initializer_list<int> signals)
                    : signal_set(signals)
                {
                }

                signal_set(fill_set f)
                {
                    fill();
                }

                signal_set(fill_set f, std::initializer_list<int> signals)
                    : signal_set(f)
                {
                    del(signals);
                }

                void clear() {
                    ::sigemptyset(&ss);
                }

                void fill() {
                    ::sigfillset(&ss);
                }

                void add(const signal_set& s) {
                    ::sigorset(&ss, &ss, const_cast<::sigset_t*>(s.get()));
                }

                void add(int s) {
                    ::sigaddset(&ss, s);
                }

                void add(std::initializer_list<int> signals) {
                    for (int s: signals)
                        add(s);
                }

                void del(int s) {
                    ::sigdelset(&ss, s);
                }

                void del(std::initializer_list<int> signals) {
                    for (int s: signals)
                        del(s);
                }

                bool is_member(int s) const {
                    return sigismember(&ss, s);
                }

                sigset_t* get() {
                    return &ss;
                }

                const sigset_t* get() const {
                    return &ss;
                }

            private:
                sigset_t ss;
        };

        using sigmask_function = int (*)(int, const sigset_t*, sigset_t*);

        template<sigmask_function f>
        class signal_mask {
            public:

                /**
                 * how: SIG_BLOCK, SIG_UNBLOCK, SIG_SETMASK
                 */
                signal_mask(int how, const signal_set& ss)
                {
                    cxxux::sys::sys_call<positiv_errno_handler>(f, how, ss.get(), old_mask.get());
                }

                signal_mask()
                {
                    cxxux::sys::sys_call<positiv_errno_handler>(f, 0, nullptr, old_mask.get());
                }

                ~signal_mask()
                {
                    try {
                        cxxux::sys::sys_call<empty_error_handler>(f, SIG_SETMASK, old_mask.get(), nullptr);
                    } catch (...) {
                    }
                }

                static
                signal_set get_current_mask() {
                    signal_mask m;
                    return m.get_old_mask();
                }

                const signal_set& get_old_mask() const {
                    return old_mask;
                }
            private:
                signal_set old_mask;
        };

        using pthread_signal_mask = signal_mask<&::pthread_sigmask>;
        using process_signal_mask = signal_mask<&::sigprocmask>;

    } /* namespace sys */
} /* namespace cxxux */

