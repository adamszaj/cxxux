/*
 * Copyright (c) 2014, 2015 Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */



#pragma once

#include <cxxux/sys/signal.h>
#include <cxxux/sys/time.h>

#include <limits>
#include <condition_variable>
#include <memory>
#include <vector>
#include <poll.h>


extern "C" {
/** forward declaration for signalfd_siginfo from sys/signalfd.h */
struct signalfd_siginfo;
}

namespace cxxux {
    namespace sys {

        template<class errhnd>
        int ppoll_eh(std::vector<::pollfd>& pollfds, const ::timespec* ts, const signal_set& mask) {
            return sys_call<errhnd>(::ppoll, pollfds.data(), pollfds.size(), ts, mask.get());
        }
        static inline
        int ppoll(std::vector<::pollfd>& pollfds, const ::timespec* ts, const signal_set& mask) {
            return ppoll_eh<system_error_handler>(pollfds, ts, mask);
        }

        class poll_signal_handler {
            public:
                virtual ~poll_signal_handler() = default;

                virtual void signal(int signo) noexcept = 0;
                virtual void signal(const signalfd_siginfo& siginfo) noexcept;
        };

        class poll {
            private:
                class poll_impl;
            public:
                class entry {
                    protected:
                        friend class cxxux::sys::poll;

                        virtual ~entry() = default;

                        /*
                         * \function events
                         * \param events poll events flags like POLLIN, POLLOUT, POLLERR
                         *        with two special values 0 and -1
                         *         0 is passed on timeout
                         *        -1 is passed on add_entry or update_entry
                         * \return new events to listen to
                         */
                        virtual short events(short events) noexcept = 0;

                        /*
                         * \function fd
                         * \return file descriptor associated with entry
                         */
                        virtual int fd() const noexcept = 0;

                        /*
                         * \function set_timeout sets timeout for entry
                         *  this finction should be called only inside events() call
                         * \throw std::bad_alloc
                         */
                        void set_timeout(const cxxux::sys::timespec& to) {
                            get_poll()->set_timeout(to, this);
                        }

                        /*
                         */
                        void clear_timeout() {
                            get_poll()->clear_timeout(this);
                        }

                        /*
                         * \brief accessor to the owner poll object
                         * \return pointer to the owner or null if entry is not added to the poll object
                         * pointer is valid only if used from inside of events() function
                         */
                        poll* get_poll() const noexcept {
                            return poll_;
                        }

                        ssize_t get_index() const noexcept {
                            return index_;
                        }

                    private:
                        void set_poll(poll* p = nullptr) noexcept { poll_ = p; }
                        void set_index(size_t i) noexcept { index_ = i; }
                        void reset_index() noexcept { index_ = -1; }

                        ssize_t index_  {-1};
                        poll*   poll_   {nullptr};
                };

                /**
                 *
                 */
                poll();
                /**
                 *
                 */
                ~poll();

                poll(const poll&) = delete;
                poll& operator= (const poll&) = delete;

                void add_signal_handler(const signal_set& mask, poll_signal_handler& shnd);
                void remove_signal_handler(poll_signal_handler& shnd);
                /**
                 * \funciton add_entry
                 * \brief adds poll entry to the poll
                 * \param entry (not owning pointer)
                 */
                void add_entry    (entry& entry);
                void remove_entry (entry& entry);
                void update_entry (entry& entry);
                void quit();

                void operator()();
                void operator()(const cxxux::sys::signal_set& mask);

            protected:
                void set_timeout(const cxxux::sys::timespec& to, entry* e);
                void clear_timeout(entry* e);
            private:
                class poll_impl;
                std::unique_ptr<poll_impl> pimpl;
        };

    } /* namespace sys */
} /* namespace cxxux */

