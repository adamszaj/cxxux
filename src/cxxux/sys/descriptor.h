/*
 * Copyright (c) 2014, 2015 Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */



#pragma once

// project
#include "sys_call.h"

// Linux
#include <unistd.h> // for int ::close(int)
#include <sys/ioctl.h>
#include <fcntl.h>

// c++
#include <memory>

namespace cxxux {
    namespace sys {

        template<class errhnd, class T>
        int ioctl_eh(int fd, int cmd, T param) {
            return sys_call<errhnd>(::ioctl, fd, cmd, param);
        }

        template<class errhnd, class T>
        int fcntl_eh(int fd, int cmd, T param) {
            return sys_call<errhnd>(::fcntl, fd, cmd, param);
        }

        template<class errhnd>
        int dup_eh(int oldfd, int newfd) {
            return sys_call<errhnd>(::dup2, oldfd, newfd);
        }

        template<class errhnd>
        int dup_eh(int oldfd, int newfd, int flags) {
            return sys_call<errhnd>(::dup3, oldfd, newfd, flags);
        }

        template<class T>
        int ioctl(int fd, int cmd, T param) {
            return ioctl_eh<system_error_handler>(fd, cmd, param);
        }

        template<class T>
        int fcntl(int fd, int cmd, T param) {
            return fcntl<system_error_handler>(fd, cmd, param);
        }

        static inline
        int dup(int oldfd, int newfd) {
            return dup_eh<system_error_handler>(oldfd, newfd);
        }

        static inline
        int dup(int oldfd, int newfd, int flags) {
            return dup_eh<system_error_handler>(oldfd, newfd, flags);
        }

        class unique_descriptor {
            public:
                // tag structs
                struct close_at_destroy_tag{};
                struct dont_close_at_destroy_tag{};

                // deleted: copy constructor and copy assign operator
                unique_descriptor(const unique_descriptor&) = delete;
                unique_descriptor& operator=(const unique_descriptor& usd) = delete;

                // default constructor
                unique_descriptor() noexcept
                    : fd{-1}, close_at_destroy{true} {}

                ~unique_descriptor() {
                    try {
                        if (close_at_destroy)
                            close(exchange(-1));
                    } catch (...) {
                        // TODO do some action... maybe call terminate or assert
                    }
                }

                explicit unique_descriptor(int __fd) noexcept
                    : fd{__fd}, close_at_destroy{true} {}

                explicit unique_descriptor(int __fd, close_at_destroy_tag cad) noexcept
                    : fd{__fd}, close_at_destroy{true} {}

                explicit unique_descriptor(int __fd, dont_close_at_destroy_tag cad) noexcept
                    : fd{__fd}, close_at_destroy{false} {}

                unique_descriptor(unique_descriptor&& usd) noexcept
                    : fd{usd.release()}, close_at_destroy{usd.close_at_destroy} {}

                unique_descriptor& operator=(unique_descriptor&& usd) {
                    if (this != &usd) {
                        int newfd = usd.release();
                        if (newfd != fd) {
                            int __fd = exchange(newfd);
                            if (__fd != -1 && close_at_destroy)
                                close(__fd);
                            close_at_destroy = usd.close_at_destroy;
                        }
                    }
                    return *this;
                }

                int release() {
                    return exchange();
                }

                void close() {
                    close(exchange());
                }

                int get() const {
                    return fd;
                }

                explicit operator int() const {
                    return fd;
                }

                template<class errhnd, class T> int ioctl_eh(int cmd, T param) {
                    return cxxux::sys::ioctl_eh<errhnd>(get(), cmd, param);
                }

                template<class errhnd, class T> int fcntl_eh(int cmd, T param) {
                    return cxxux::sys::fcntl_eh<errhnd>(get(), cmd, param);
                }

                template<class T> int ioctl(int cmd, T param) {
                    return ioctl_eh<system_error_handler>(cmd, param);
                }

                template<class T> int fcntl(int cmd, T param) {
                    return fcntl_eh<system_error_handler>(cmd, param);
                }

                template<class errhnd>
                int dup_eh(int newfd) {
                    return cxxux::sys::dup_eh<errhnd>(get(), newfd);
                }

                template<class errhnd>
                int dup_eh(int newfd, int flags) {
                    return cxxux::sys::dup_eh<errhnd>(get(), newfd, flags);
                }

                int dup(int newfd) {
                    return dup_eh<system_error_handler>(newfd);
                }

                int dup(int newfd, int flags) {
                    return dup_eh<system_error_handler>(newfd, flags);
                }

            protected:
                int exchange(int new_val = -1) {
                    int old_val = fd;
                    fd = new_val;
                    return old_val;
                }
            private:
                int     fd;
                bool    close_at_destroy;

                void close(int __fd) {
                    if (__fd != -1) {
                        sys_call<system_error_handler>(::close, __fd);
                    }
                }
        };

        class shared_descriptor {
            public:
                shared_descriptor()
                    : usd_ptr{std::make_shared<unique_descriptor>()}, fd{-1}
                {
                }

                explicit shared_descriptor(int __fd)
                    : usd_ptr{std::make_shared<unique_descriptor>(__fd)}, fd{__fd}
                {
                }

                shared_descriptor(const shared_descriptor& ssd)
                    : usd_ptr{ssd.usd_ptr}, fd{usd_ptr->get()}
                {
                }

                shared_descriptor(shared_descriptor&& ssd)
                    : usd_ptr{std::move(ssd.usd_ptr)}, fd{usd_ptr->get()}
                {
                    ssd.clear();
                }

                explicit shared_descriptor(unique_descriptor&& usd)
                    : usd_ptr(std::make_shared<unique_descriptor>(usd.release())), fd{usd_ptr->get()}
                {
                }

                void reset() {
                    usd_ptr = std::make_shared<unique_descriptor>();
                    fd = -1;
                }

                shared_descriptor& operator= (shared_descriptor&& ssd) {
                    usd_ptr = std::move(ssd.usd_ptr);
                    fd = usd_ptr->get();
                    ssd.clear();
                    return *this;
                }

                shared_descriptor& operator= (const shared_descriptor& ssd) {
                    usd_ptr = ssd.usd_ptr;
                    fd = usd_ptr->get();
                    return *this;
                }

                void close() {
                    fd = -1;
                    usd_ptr->close();
                }

                int get() const {
                    return fd;
                }

                explicit operator int() const {
                    return get();
                }

                template<class errhnd, class T> int ioctl_eh(int cmd, T param) {
                    return usd_ptr->ioctl_eh<errhnd>(cmd, param);
                }

                template<class errhnd, class T> int fcntl_eh(int cmd, T param) {
                    return usd_ptr->fcntl_eh<errhnd>(cmd, param);
                }

                template<class T> int ioctl(int cmd, T param) {
                    return ioctl_eh<system_error_handler>(cmd, param);
                }

                template<class T> int fcntl(int cmd, T param) {
                    return fcntl_eh<system_error_handler>(cmd, param);
                }

                template<class errhnd>
                int dup_eh(int newfd) {
                    return usd_ptr->dup_eh<errhnd>(newfd);
                }

                template<class errhnd>
                int dup_eh(int newfd, int flags) {
                    return usd_ptr->dup_eh<errhnd>(newfd, flags);
                }

                int dup(int newfd) {
                    return dup_eh<system_error_handler>(newfd);
                }

                int dup(int newfd, int flags) {
                    return dup_eh<system_error_handler>(newfd, flags);
                }
                ////////////////////
            private:
                std::shared_ptr<unique_descriptor> usd_ptr;
                // copy of file_desc value, for quicker access
                int     fd;


                void clear() {
                    usd_ptr.reset();
                    fd = -1;
                }
        };

    } /* namespace sys */
} /* namespace cxxux */

