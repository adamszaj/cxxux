/*
 * Copyright (c) 2014, 2015 Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */



#pragma once

#include <unistd.h>
#include <iostream>
#include <cerrno>
#include <cstring>
#include <system_error>
#include <cerrno>

namespace cxxux {
    namespace sys {

        struct errno_storage {
            int err;

            /// store current errno value
            errno_storage() : err(errno) {}

            /// store given errno value
            explicit errno_storage(int e) : err(e) {}

            operator int() const {
                return err;
            }

            int code() const {
                return err;
            }

            const char* c_message() const {
                return std::strerror(err);
            }
        };

        inline static std::ostream& operator << (std::ostream& out, const errno_storage& e) {
            return out << "[" << e.c_message() << "]";
        }

        static inline
        std::error_code sys_error_code(int err) {
            return {err, std::system_category()};
        }

    } /* namespace sys */
} /* namespace cxxux */

/**
// macro for throwing exceptions where errno value does matter.
// ex:
// int fd = open("file-name.txt", O_RDONLY);
// if (fd == -1)
//     sys_throw(std::system_error(std::error_code{cxxux_errno, std::system_category{}}, "Can't open file" ));
//
*/

#define sys_throw(exc)                              \
        do {                                        \
            cxxux::sys::errno_storage cxxux_errno;  \
            throw exc;                              \
        } while(0)


