/*
 * Copyright (c) 2014, 2015 Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */



#pragma once

#include "sys_call.h"
#include <sys/stat.h>
#include <sys/uio.h>

namespace cxxux {
    namespace sys {

        struct noent_error_handler {
            template<class R> static R check(R v) {
                errno_storage err;
                if ((v == -1) && (int(err) != ENOENT))
                    throw std::system_error(sys_error_code(err));
                return v;
            }
        };

        class file_stat {
            public:
                explicit file_stat(const std::string& __path) {
                    sys_call<system_error_handler>(::lstat, __path.c_str(), &s);
                }

                explicit file_stat(int __fd) {
                    sys_call<system_error_handler>(::fstat, __fd, &s);
                }

                static bool exist(const std::string& __path) {
                    struct ::stat __stat;
                    if (sys_call<noent_error_handler>(::lstat, __path.c_str(), &__stat) == -1)
                        return false;
                    return true;
                }

                bool is_file() const { return S_ISREG(s.st_mode); }
                bool is_directory() const { return S_ISDIR(s.st_mode); }
                bool is_char_device() const { return S_ISCHR(s.st_mode); }
                bool is_block_device() const { return S_ISBLK(s.st_mode); }
                bool is_fifo() const { return S_ISFIFO(s.st_mode); }
                bool is_link() const { return S_ISLNK(s.st_mode); }
                bool is_socket() const { return S_ISSOCK(s.st_mode); }
                off_t file_size() const { return s.st_size; }
                nlink_t nlink() const { return s.st_nlink; }
                dev_t dev() const { return s.st_dev; }
                dev_t rdev() const { return s.st_rdev; }
                uid_t uid() const { return s.st_uid; }
                gid_t gid() const { return s.st_gid; }
                blksize_t block_size() const { return s.st_blksize; }
                blkcnt_t block_count() const { return s.st_blocks; }

                mode_t mode() const { return s.st_mode; }
                ino_t inode_num() const { return s.st_ino; }
                time_t access_time() const { return s.st_atime; }
                time_t modification_time() const { return s.st_mtime; }
                time_t status_change_time() const { return s.st_ctime; }
                const struct ::stat* get() const { return &s; }
            private:
                struct stat s;
        }; /* class file_stat */

    } /* namespace sys */
} /* namespace cxxux */

