/*
 * Copyright (c) 2014, 2015 Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */



#pragma once
#include <ctime>
#include <iostream>
#include <iomanip>

namespace cxxux {
    namespace sys {

        struct timespec {
            ::timespec value;

            timespec() : value{0,0} {}


            static timespec now(clockid_t clock_id) {
                timespec ts;
                clock_gettime(clock_id, &ts.value);
                return ts;
            }

            static timespec rt_now() {
                return now(CLOCK_REALTIME);
            }

            static timespec monotonic_now() {
                return now(CLOCK_MONOTONIC);
            }
        };

        static inline
        timespec operator -(const timespec& lhs, const timespec& rhs)  {
            timespec res;
            res.value.tv_sec = lhs.value.tv_sec - rhs.value.tv_sec;
            if (lhs.value.tv_nsec < rhs.value.tv_nsec) {
                res.value.tv_nsec = (lhs.value.tv_nsec + 1000000000UL) - rhs.value.tv_nsec;
            } else {
                res.value.tv_nsec = lhs.value.tv_nsec - rhs.value.tv_nsec;
            }
            return res;
        }

        static inline
        std::ostream& operator<< (std::ostream& out, const timespec& ts) {
            auto f = out.fill('0');
            out << ts.value.tv_sec <<'.'<< std::setw(9) << ts.value.tv_nsec;
            out.fill(f);
            return out;
        }

        static inline
        bool operator < (const timespec& lhs, const timespec& rhs) {
            if (lhs.value.tv_sec == rhs.value.tv_sec)
                return lhs.value.tv_nsec < rhs.value.tv_nsec;
            return lhs.value.tv_sec < rhs.value.tv_sec;
        }

        static inline
        bool operator > (const timespec& lhs, const timespec& rhs) {
            return rhs < lhs;
        }

        static inline
        bool operator == (const timespec& lhs, const timespec& rhs) {
            return !((lhs < rhs) || (rhs < lhs));
        }

        static inline
        bool operator >= (const timespec& lhs, const timespec& rhs) {
            return ! (lhs < rhs);
        }

        static inline
        bool operator <= (const timespec& lhs, const timespec& rhs) {
            return ! (lhs > rhs);
        }


    } /* namespace sys */
} /* namespace cxxux */
