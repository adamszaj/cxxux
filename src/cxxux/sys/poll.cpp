/*
 * Copyright (c) 2014, 2015 Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */



#include "poll.h"
#include "signal.h"
#include <mutex>
#include <thread>
#include <set>
#include <queue>

#include <sys/eventfd.h>
#include <sys/signalfd.h>

#include <cxxux/utils/std.h>
#include <cxxux/sys/descriptor.h>
#include <cxxux/sys/file.h>
#include <cxxux/sys/sys_call.h>

namespace cxxux {
    namespace sys {

        template<class errhnd>
        unique_descriptor eventfd_eh(unsigned int initval, int flags) {
            return unique_descriptor{sys::sys_call<errhnd>(::eventfd, initval, flags)};
        }

        static inline
            unique_descriptor eventfd(unsigned int initval, int flags) {
                return eventfd_eh<system_error_handler>(initval, flags);
            }


        template<class errhnd>
        unique_descriptor signalfd_eh(int fd, const signal_set& s, int flags) {
            return unique_descriptor{sys::sys_call<errhnd>(::signalfd, fd, s.get(), flags)};
        }

        static inline
            unique_descriptor signalfd(int fd, const signal_set& s, int flags) {
                return signalfd_eh<system_error_handler>(fd, s, flags);
            }

        class event_entry : public poll::entry {
            public:
                event_entry(uint64_t& _v)
                    : desc{ eventfd( 0, EFD_CLOEXEC | EFD_NONBLOCK) }, v(_v)
                {
                }

                // TODO check errors
                void send_event(uint64_t v = 1) {
                    cxxux::sys::write_eh<asystem_error_handler>(desc.get(), &v, sizeof(v));
                }

            protected:
                short events(short events) noexcept override {
                    if ((events > 0) && ((events & POLLIN) != 0)) {
                        read_event();
                    }
                    return POLLIN;
                }

                int fd() const noexcept override {
                    return desc.get();
                }
            private:
                unique_descriptor desc;
                uint64_t&         v;
                uint64_t read_event() const {
                    try {
                        cxxux::sys::read_eh<empty_error_handler>(desc.get(), &v, sizeof(v));
                    } catch (...) {
                    }
                    return v;
                }
        };

        void poll_signal_handler::signal(const signalfd_siginfo& siginfo) noexcept {
            signal(siginfo.ssi_signo);
        }

        class signal_entry : public poll::entry {
            public:
                signal_entry()
                    : desc{signalfd(-1, sset, SFD_CLOEXEC | SFD_NONBLOCK)}
                {
                }

                void add_handler(const signal_set& mask, poll_signal_handler& handler) {
                    sset.add(mask);
                    handlers.push_back(std::make_pair(mask, &handler));
                    desc = signalfd(desc.release(), sset, SFD_CLOEXEC | SFD_NONBLOCK );
                }

                void remove_handler(poll_signal_handler& handler) {
                    for (auto it = handlers.begin(); it != handlers.end(); ) {
                        if (it->second == &handler) {
                            it = handlers.erase(it);
                        } else {
                            ++it;
                        }
                    }
                    sset.clear();
                    for (const auto& h: handlers) {
                        sset.add(h.first);
                    }
                    desc = signalfd(desc.release(), sset, SFD_CLOEXEC | SFD_NONBLOCK );
                }

                const signal_set& get_mask() const {
                    return sset;
                }
            protected:

                short events(short events) noexcept override {
                    if ((events > 0) && ((events & POLLIN) != 0)) {
                        signalfd_siginfo sinfo;
                        auto n = ::read(desc.get(), &sinfo, sizeof(sinfo));
                        if (n == sizeof(sinfo)) {
                            for (const auto& e: handlers) {
                                if (e.first.is_member(sinfo.ssi_signo))
                                    e.second->signal(sinfo);
                            }
                        }
                    }
                    return POLLIN;
                }

                int fd() const noexcept override {
                    return desc.get();
                }

            private:
                using handler_entry = std::pair<signal_set, poll_signal_handler*>;
                using handlers_vector = std::vector<handler_entry>;

                unique_descriptor   desc;
                signal_set              sset;
                handlers_vector     handlers;
        };

        class poll::poll_impl {
            public:
                using LockGuard = std::lock_guard<std::mutex>;

                friend class cxxux::sys::poll;

                poll_impl(poll* _owner)
                    : owner{_owner}, notify{events}
                {
                    _add_entry(&notify);
                    _add_entry(&signal_handler);
                }

                ~poll_impl()
                {
                }

                struct command {
                    enum command_type { nop, add, remove, update, quit };

                    command_type    type;
                    entry*          ent{nullptr};

                    command(command_type t, entry* e = nullptr)
                        : type(t), ent(e)
                    {
                    }
                };

                struct timeout_entry {
                    cxxux::sys::timespec    timeout;
                    entry*                  ent;

                    bool operator < (const timeout_entry& toe) const {
                        if (timeout < toe.timeout)
                            return true;
                        if (timeout == toe.timeout)
                            return ent < toe.ent;
                        return false;
                    }

                    bool operator == (const timeout_entry& toe) const {
                        return (timeout == toe.timeout) && (ent == toe.ent);
                    }
                };

                void add_entry(entry& entry) {
                    push(command::add, &entry);
                    notify.send_event();
                }

                void remove_entry(entry& ent) {
                    std::unique_lock<std::mutex> lock{m};
                    entry* ent_ptr = &ent;
                    for (auto& cmd: commands) {
                        if (cmd.ent == ent_ptr) {
                            cmd.type = command::nop;
                            cmd.ent  = nullptr;
                        }
                    }
                    _push(command::remove, ent_ptr);
                    notify.send_event();
                    cv.wait(lock, [ent_ptr]() -> bool { return ent_ptr->get_index() == -1; });
                }

                void update_entry(entry& entry) {
                    push(command::update, &entry);
                    notify.send_event();
                }

                void quit() {
                    push(command::quit, nullptr);
                    notify.send_event();
                }

                void set_timeout(const cxxux::sys::timespec& to, entry* e) {
                    /** it is safe to use cast here since this function is valid only ehen index is >= 0 */
                    auto i = static_cast<size_t>(e->get_index());
                    if (i < entries.size()) {
                        entries[i].timeout = to;
                        /** can throw bad_alloc */
                        timeouts.insert(entries[i]);
                    }
                }

                void clear_timeout(entry* e) {
                    /** it is safe to use cast here since this function is valid only ehen index is >= 0 */
                    auto i = static_cast<size_t>(e->get_index());
                    if (i < entries.size()) {
                        timeouts.erase(entries[i]);
                    }
                }

                void add_signal_handler(const signal_set& mask, poll_signal_handler& shnd) {
                    signal_handler.add_handler(mask, shnd);
                }
                void remove_signal_handler(poll_signal_handler& shnd) {
                    // TODO
                }
            private:
                poll*                           owner;
                uint64_t                        events;
                event_entry                     notify;
                signal_entry                    signal_handler;

                std::vector<::pollfd>           fds;
                std::vector<timeout_entry>      entries;
                std::set<timeout_entry>         timeouts;
                std::deque<command>             commands;

                std::mutex                      m;
                std::condition_variable         cv;

                void loop(const signal_set& sset);
                void loop() { loop(signal_handler.get_mask()); }

                void push(command::command_type type, entry* ent) {
                    LockGuard lock{m};
                    _push(type, ent);
                }
                void _push(command::command_type type, entry* ent) {
                    commands.push_back({type, ent});
                }

                void _add_entry(entry* e);
                void _remove_entry(entry* e);
                void _update_entry(entry* e);

                void    del_desc(size_t i);
                int     iterate(const signal_set& sset);
                int     run_commands();
                void    run_events(int n);
                void    run_timeouts();

                void    clear_entries();
                void    reserve_entries();
        };

        poll::poll()
            : pimpl{std::make_unique<poll_impl>(this)}
        {
        }

        poll::~poll() = default;

        void poll::add_signal_handler(const signal_set& mask, poll_signal_handler& shnd) {
            pimpl->add_signal_handler(mask, shnd);
        }

        void poll::remove_signal_handler(poll_signal_handler& shnd) {
            pimpl->remove_signal_handler(shnd);
        }

        void poll::quit() {
            pimpl->quit();
        }
        void poll::add_entry(entry& ent) {
            pimpl->add_entry(ent);
        }

        void poll::remove_entry(entry& ent) {
            pimpl->remove_entry(ent);
        }

        void poll::update_entry(entry& ent) {
            pimpl->update_entry(ent);
        }

        void poll::set_timeout(const cxxux::sys::timespec& to, entry* e) {
            pimpl->set_timeout(to, e);
        }
        void poll::clear_timeout(entry* e) {
            pimpl->clear_timeout(e);
        }

        void poll::operator()() {
            pimpl->loop();
        }

        void poll::operator()(const cxxux::sys::signal_set& sset) {
            pimpl->loop(sset);
        }

        void poll::poll_impl::loop (const cxxux::sys::signal_set& sset) {

            try {
                if (run_commands() == -1)
                    return;

                while (true)
                    if (iterate(sset) == -1)
                        break;

            } catch (...) {
                std::cerr << "err\n";
            }
            clear_entries();
        }

        void poll::poll_impl::clear_entries() {
            {
                LockGuard lock{m};
                while (!entries.empty()) {
                    del_desc(entries.size()-1);
                }
            }
            cv.notify_all();
        }

        void poll::poll_impl::reserve_entries() {
            if (entries.size() == entries.capacity()) {
                size_t reserve = 64;
                if (entries.size() > 0) {
                    reserve = entries.size() * 2;
                }
                fds.reserve(reserve);
                entries.reserve(reserve);
            }
        }

        void poll::poll_impl::_add_entry(entry* entry) {

            entry->set_poll(owner);
            entry->set_index(entries.size());

            const int fd = entry->fd();
            const short events = entry->events(-1);
            if ((fd > -1) && (events != -1)) {
                ::pollfd pd { fd, events, 0 };

                reserve_entries();

                entries.push_back({{}, entry});
                fds.push_back(pd);
            } else {
                entry->set_poll();
                entry->reset_index();
            }
        }

        void poll::poll_impl::_remove_entry(entry* entry) {
            ssize_t index = entry->get_index();
            if (index >= 0 && (index < static_cast<ssize_t>(entries.size()))) {
                del_desc(index);
            }
        }

        void poll::poll_impl::_update_entry(entry* entry) {
            ssize_t index = entry->get_index();
            if (index >= 0 && (index < static_cast<ssize_t>(entries.size()))) {
                fds[index].fd     = entry->fd();
                fds[index].events = entry->events(-1);
            }
        }

        void poll::poll_impl::del_desc(size_t index) {
            if ((index+1) < entries.size()) {
                fds[index]      = fds.back();
                entries[index].ent->reset_index();
                entries[index].ent->set_poll();
                entries[index] = entries.back();
                entries[index].ent->set_index(index);
            }
            fds.pop_back();
            entries.pop_back();
        }

        struct interrupt_error_handler {
            template<class T>
            static T check(T v) {
                if ((v == -1) && (errno == EINTR))
                    return -1;
                return system_error_handler::check(v);
            }
        };

        int poll::poll_impl::iterate(const signal_set& sset) {
            ::timespec* ts_ptr{nullptr};
            ::timespec ts{0,0};

            if (!timeouts.empty()) {
                auto now = timespec::monotonic_now();
                auto to = timeouts.begin()->timeout;
                if (to <= now) {
                    ts = {0,0};
                } else {
                    ts = (to - now).value;
                }
                ts_ptr = &ts;
            }

            /** can throw */
            int n = ppoll_eh<interrupt_error_handler>(fds, ts_ptr, sset);

            switch (n) {
                default:
                    run_events(n);
                case 0:
                    run_timeouts();
                    break;
                case -1:
                    break;
            }
            if (events) {
                events = 0;
                return run_commands();
            }
            return 0;
        }

        int poll::poll_impl::run_commands() {
            int ret = 0;
            {
                LockGuard lock{m};
                while (!commands.empty()) {
                    auto cmd = commands.front();
                    commands.pop_front();
                    switch (cmd.type) {
                        case command::add:
                            _add_entry(cmd.ent);
                            break;
                        case command::remove:
                            _remove_entry(cmd.ent);
                            break;
                        case command::update:
                            _update_entry(cmd.ent);
                            break;
                        case command::quit:
                            ret = -1;
                            break;
                        case command::nop:
                            break;
                    }
                }
            }
            cv.notify_all();
            return ret;
        }

        void poll::poll_impl::run_events(int n) {
            for (size_t i = 0; (n > 0) && (i < fds.size()); ) {
                short e = fds[i].revents;
                fds[i].revents = 0;
                if (e) {
                    e = entries[i].ent->events(e);
                    if (e == -1) {
                        del_desc(i);
                    } else {
                        fds[i].events = e;
                        ++i;
                    }
                    --n;
                } else {
                    ++i;
                }
            }
        }

        void poll::poll_impl::run_timeouts() {
            auto now = timespec::monotonic_now();
            // timeout
            while (!timeouts.empty()) {
                auto it = timeouts.begin();
                if (it->timeout <= now) {
                    auto ent = it->ent;
                    timeouts.erase(it);

                    if (ent) {
                        short e = ent->events(0);
                        size_t i = ent->get_index();
                        if (e == -1) {
                            del_desc(i);
                        } else {
                            fds[i].events = e;
                            fds[i].revents = 0;
                        }
                    }
                } else {
                    break;
                }
            }
        }

    } /* namespace sys */

} /* namespace cxxux */
