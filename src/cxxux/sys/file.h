/*
 * Copyright (c) 2014, 2015 Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */

#pragma once

#include "sys_call.h"
#include "descriptor.h"
#include <sys/uio.h>
#include <vector>

namespace cxxux {
    namespace sys {

        template<class errhnd>
        ssize_t write_eh (int fd, const void* data, size_t size) {
            return sys_call<errhnd>(::write, fd, data, size);
        }

        template<class errhnd>
        ssize_t pwrite_eh (int fd, const void* data, size_t size, off_t offset) {
            return sys_call<errhnd>(::pwrite, fd, data, size, offset);
        }

        template<class errhnd>
        ssize_t writev_eh (int fd, const struct iovec* iov, size_t cnt) {
            return sys_call<errhnd>(::writev, fd, iov, cnt);
        }

        template<class errhnd>
        ssize_t pwritev_eh (int fd, const struct iovec* iov, size_t cnt, off_t offset) {
            return sys_call<errhnd>(::pwritev, fd, iov, cnt, offset);
        }

        template<class errhnd>
        ssize_t read_eh (int fd, void* data, size_t size) {
            return sys_call<errhnd>(::read, fd, data, size);
        }

        template<class errhnd>
        ssize_t pread_eh (int fd, void* data, size_t size, off_t offset) {
            return sys_call<errhnd>(::pread, fd, data, size, offset);
        }

        template<class errhnd>
        ssize_t readv_eh (int fd, const struct iovec* iov, size_t cnt) {
            return sys_call<errhnd>(::readv, fd, iov, cnt);
        }

        template<class errhnd>
        ssize_t preadv_eh (int fd, const struct iovec* iov, size_t cnt, off_t offset) {
            return sys_call<errhnd>(::preadv, fd, iov, cnt, offset);
        }

        template<class errhnd>
        unique_descriptor open_eh(const std::string& file_name, int flags, mode_t mode) {
            return unique_descriptor{sys_call<errhnd>(::open, file_name.c_str(), flags, mode)};
        }

        template<class errhnd>
        int unlink_eh(const std::string& file_name) {
            return sys_call<errhnd>(::unlink, file_name.c_str());
        }

        static inline
        int unlink(const std::string& file_name) {
            return unlink_eh<system_error_handler>(file_name);
        }

        template<class errhnd>
        unique_descriptor open_eh(const std::string& file_name, int flags) {
            return open_eh<errhnd>(file_name, flags, 0);
        }

        using cxxux::sys::system_error_handler;
        ////////////////////////////////////////////////////////////////////////
        static inline ssize_t write (int fd, const void* data, size_t size) {
            return write_eh<system_error_handler>(fd, data, size);
        }

        static inline ssize_t pwrite (int fd, const void* data, size_t size, off_t offset) {
            return pwrite_eh<system_error_handler>(fd, data, size, offset);
        }

        static inline ssize_t writev (int fd, const struct iovec* iov, size_t cnt) {
            return writev_eh<system_error_handler>(fd, iov, cnt);
        }

        static inline ssize_t pwritev (int fd, const struct iovec* iov, size_t cnt, off_t offset) {
            return pwritev_eh<system_error_handler>(fd, iov, cnt, offset);
        }

        static inline ssize_t read (int fd, void* data, size_t size) {
            return read_eh<system_error_handler>(fd, data, size);
        }

        static inline ssize_t pread (int fd, void* data, size_t size, off_t offset) {
            return pread_eh<system_error_handler>(fd, data, size, offset);
        }

        static inline ssize_t readv (int fd, const struct iovec* iov, size_t cnt) {
            return readv_eh<system_error_handler>(fd, iov, cnt);
        }

        static inline ssize_t preadv (int fd, const struct iovec* iov, size_t cnt, off_t offset) {
            return preadv_eh<system_error_handler>(fd, iov, cnt, offset);
        }

        static inline unique_descriptor open(const std::string& file_name, int flags, mode_t mode) {
            return open_eh<system_error_handler>(file_name.c_str(), flags, mode);
        }

        static inline unique_descriptor open(const std::string& file_name, int flags) {
            return open(file_name, flags, 0);
        }

        template<class Desc = unique_descriptor, class EH = system_error_handler, class AEH = asystem_error_handler>
        class file {
            public:

                using default_error_handler = EH;
                using async_error_handler   = AEH;
                using descriptor_type       = Desc;

                explicit file(descriptor_type d)
                    : desc(std::move(d))
                {
                }

                file() = default;

                file(file&&) = default;
                file(const file& ) = default;

                file& operator= (file&&) = default;
                file& operator= (const file& ) = default;

                //
                // read_eh()
                //
                template<class errhnd>
                ssize_t read_eh(void* data, size_t size) {
                    return cxxux::sys::read_eh<errhnd>(desc.get(), data, size);
                }

                //
                // write_eh()
                //
                template<class errhnd>
                ssize_t write_eh(const void* data, size_t size) {
                    return cxxux::sys::write_eh<errhnd>(desc.get(), data, size);
                }

                //
                // read()
                //
                ssize_t read(void* data, size_t size) {
                    return read_eh<default_error_handler>(data, size);
                }

                ssize_t aread(void* data, size_t size) {
                    return read_eh<async_error_handler>(data, size);
                }

                //
                // write()
                //
                ssize_t write(const void* data, size_t size) {
                    return write_eh<default_error_handler>(data, size);
                }

                ssize_t awrite(const void* data, size_t size) {
                    return write_eh<async_error_handler>(data, size);
                }

                //
                // readv_eh()
                //
                template<class errhnd>
                ssize_t readv_eh(const struct iovec* iov, unsigned iovcnt) {
                    return cxxux::sys::readv_eh<errhnd>(desc.get(), iov, iovcnt);
                }

                //
                // writev_eh()
                //
                template<class errhnd>
                ssize_t writev_eh(const struct iovec* iov, unsigned iovcnt) {
                    return cxxux::sys::writev_eh<errhnd>(desc.get(), iov, iovcnt);
                }

                //
                // readv()
                //
                ssize_t readv(const struct iovec* iov, unsigned iovcnt) {
                    return readv_eh<default_error_handler>(iov, iovcnt);
                }
                ssize_t areadv(const struct iovec* iov, unsigned iovcnt) {
                    return readv_eh<async_error_handler>(iov, iovcnt);
                }

                //
                // writev()
                //
                ssize_t writev(const struct iovec* iov, unsigned iovcnt) {
                    return writev_eh<default_error_handler>(iov, iovcnt);
                }

                ssize_t awritev(const struct iovec* iov, unsigned iovcnt) {
                    return writev_eh<async_error_handler>(iov, iovcnt);
                }
                //
                // readv_eh ()
                //
                template<class errhnd>
                ssize_t readv_eh(const std::vector<iovec>& iov, unsigned off, unsigned n) {
                    check_range(iov.size(), off, n);
                    return readv_eh<errhnd>(iov.data()+off, n);
                }

                template<class errhnd>
                ssize_t readv_eh(const std::vector<iovec>& iov, unsigned off) {
                    return readv_eh<errhnd>(iov, off, iov.size()-off);
                }

                template<class errhnd>
                ssize_t readv_eh(const std::vector<iovec>& iov) {
                    return readv_eh<errhnd>(iov, 0, iov.size());
                }

                //
                // readv ()
                //
                ssize_t readv(const std::vector<iovec>& iov, unsigned off, unsigned n) {
                    return readv_eh<default_error_handler>(iov, off, n);
                }

                ssize_t readv(const std::vector<iovec>& iov, unsigned off) {
                    return readv_eh<default_error_handler>(iov, off);
                }

                ssize_t readv(const std::vector<iovec>& iov) {
                    return readv_eh<default_error_handler>(iov);
                }

                ssize_t areadv(const std::vector<iovec>& iov, unsigned off, unsigned n) {
                    return readv_eh<async_error_handler>(iov, off, n);
                }

                ssize_t areadv(const std::vector<iovec>& iov, unsigned off) {
                    return readv_eh<async_error_handler>(iov, off);
                }

                ssize_t areadv(const std::vector<iovec>& iov) {
                    return readv_eh<async_error_handler>(iov);
                }


                //
                // writev_eh ()
                //
                template<class errhnd>
                ssize_t writev_eh(const std::vector<iovec>& iov, unsigned off, unsigned n) {
                    check_range(iov.size(), off, n);
                    return writev_eh<errhnd>(iov.data()+off, n);
                }

                template<class errhnd>
                ssize_t writev_eh(const std::vector<iovec>& iov, unsigned off) {
                    return writev_eh<errhnd>(iov, off, iov.size()-off);
                }

                template<class errhnd>
                ssize_t writev_eh(const std::vector<iovec>& iov) {
                    return writev_eh<errhnd>(iov, 0, iov.size());
                }
                //
                // writev ()
                //
                ssize_t writev(const std::vector<iovec>& iov, unsigned off, unsigned n) {
                    return writev_eh<default_error_handler>(iov, off, n);
                }

                ssize_t writev(const std::vector<iovec>& iov, unsigned off) {
                    return writev_eh<default_error_handler>(iov, off);
                }

                ssize_t writev(const std::vector<iovec>& iov) {
                    return writev_eh<default_error_handler>(iov);
                }

                ssize_t awritev(const std::vector<iovec>& iov, unsigned off, unsigned n) {
                    return writev_eh<async_error_handler>(iov, off, n);
                }

                ssize_t awritev(const std::vector<iovec>& iov, unsigned off) {
                    return writev_eh<async_error_handler>(iov, off);
                }

                ssize_t awritev(const std::vector<iovec>& iov) {
                    return writev_eh<async_error_handler>(iov);
                }

                //
                /// read_eh (std::vector<T>)
                //
                template<class errhnd, class T>
                ssize_t read_eh(std::vector<T>& v, size_t off, size_t n) {
                    static_assert(std::is_pod<T>::value, "std::vector::value_type is not a POD type");
                    check_range(v.size(), off, n);
                    if (n > 0)
                        return read_eh<errhnd>(v.data()+off, sizeof(T)*n);
                    return 0;
                }

                template<class errhnd, class T>
                ssize_t read_eh(std::vector<T>& v, size_t off) {
                    return read_eh<errhnd>(v, off, v.size()-off);
                }

                template<class errhnd, class T>
                ssize_t read_eh(std::vector<T>& v) {
                    return read_eh<errhnd>(v, 0, v.size());
                }
                //
                /// read (std::vector<T>)
                //
                template<class T>
                ssize_t read(std::vector<T>& v, size_t off, size_t n) {
                    return read_eh<default_error_handler>(v, off, n);
                }

                template<class T>
                ssize_t read(std::vector<T>& v, size_t off) {
                    return read_eh<default_error_handler>(v, off);
                }

                template<class T>
                ssize_t read(std::vector<T>& v) {
                    return read_eh<default_error_handler>(v);
                }

                template<class T>
                ssize_t aread(std::vector<T>& v, size_t off, size_t n) {
                    return read_eh<async_error_handler>(v, off, n);
                }

                template<class T>
                ssize_t aread(std::vector<T>& v, size_t off) {
                    return read_eh<async_error_handler>(v, off);
                }

                template<class T>
                ssize_t aread(std::vector<T>& v) {
                    return read_eh<async_error_handler>(v);
                }

                //
                /// write_eh (std::vector<T>)
                //
                template<class errhnd, class T>
                ssize_t write_eh(const std::vector<T>& v, size_t off, size_t n) {
                    static_assert(std::is_pod<T>::value, "std::vector::value_type is not a POD type");
                    check_range(v.size(), off, n);
                    if (n > 0)
                        return write_eh<errhnd>(v.data()+off, sizeof(T)*n);
                    return 0;
                }

                template<class errhnd, class T>
                ssize_t write_eh(const std::vector<T>& v, size_t off) {
                    return write_eh<errhnd>(v, off, v.size()-off);
                }

                template<class errhnd, class T>
                ssize_t write_eh(const std::vector<T>& v) {
                    return write_eh<errhnd>(v, 0, v.size());
                }

                //
                /// write (std::vector<T>)
                //
                template<class T>
                ssize_t write(const std::vector<T>& v, size_t off, size_t n) {
                    return write_eh<default_error_handler>(v, off, n);
                }

                template<class T>
                ssize_t write(const std::vector<T>& v, size_t off) {
                    return write_eh<default_error_handler>(v, off);
                }

                template<class T>
                ssize_t write(const std::vector<T>& v) {
                    return write_eh<default_error_handler>(v);
                }

                template<class T>
                ssize_t awrite(const std::vector<T>& v, size_t off, size_t n) {
                    return write_eh<async_error_handler>(v, off, n);
                }

                template<class T>
                ssize_t awrite(const std::vector<T>& v, size_t off) {
                    return write_eh<async_error_handler>(v, off);
                }

                template<class T>
                ssize_t awrite(const std::vector<T>& v) {
                    return write_eh<async_error_handler>(v);
                }


                /// ioctl
                template<class errhnd, class T>
                int ioctl_eh(int req, T param) {
                    return desc.ioctl_eh<errhnd>(req, param);
                }

                template<class T>
                int ioctl(int req, T param) {
                    return ioctl_eh<default_error_handler>(req, param);
                }

                /// fcntl
                template<class errhnd, class T>
                int fcntl_eh(int req, T param) {
                    return desc.fcntl_eh<errhnd>(req, param);
                }

                template<class T>
                int fcntl(int req, T param) {
                    return fcntl_eh<default_error_handler>(req, param);
                }

                descriptor_type& fd() {
                    return desc;
                }

                const descriptor_type& cfd() const {
                    return desc;
                }

                const descriptor_type& fd() const {
                    return cfd();
                }

                void close() { desc.close(); }

            protected:

                descriptor_type fd(descriptor_type d) {
                    std::swap(desc, d);
                    return d;
                }

                // TODO make it util function
                static void check_range(size_t size, size_t off, size_t n) {
                    if (size < off)
                        throw std::length_error("Offset (" + std::to_string(off) + ") is bigger than size (" + std::to_string(size) + ")");

                    if (size < (off+n))
                        throw std::length_error("the end of given range (" + std::to_string(off+n) + ") is bigger than size (" + std::to_string(size) + ")");
                }

            private:
                /* data */
                descriptor_type desc;
        }; /* class file */

        using unique_file = file<unique_descriptor>;
        using shared_file = file<shared_descriptor>;

    } /* namespace sys */

} /* namespace cxxux */
