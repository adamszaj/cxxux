/*
 * Copyright (c) 2014, 2015 Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */



#pragma once
#include "system.h"
#include <utility>
#include <typeinfo>

namespace cxxux {
    namespace sys {

        struct empty_error_handler {
            template<class R> static R check(R v) { return v; }
        };

        struct system_error_handler {
            template<class R> static R check(R v) {
                if (v == -1)
                    sys_throw(std::system_error(sys_error_code(cxxux_errno)));
                return v;
            }
        };

        struct asystem_error_handler {
            template<class R> static R check(R v) {
                if (v == -1) {
                    errno_storage err;
                    switch (err.code()) {
                        case EAGAIN:
#if EAGAIN != EWOULDBLOCK
                        case EWOULDBLOCK:
#endif
                            return 0;
                        default:
                            break;
                    }
                    sys_throw(std::system_error(sys_error_code(err)));
                } else if (v == 0) {
                    return -1;
                }
                return v;
            }
        };

        struct positiv_errno_handler {
            template<class R> static R check(R v) {
                if (v > 0)
                    sys_throw(std::system_error(sys_error_code(v)));
                return v;
            }
        };

        struct negativ_errno_handler {
            template<class R> static R check(R v) {
                if (v < 0)
                    sys_throw(std::system_error(sys_error_code(-v)));
                return v;
            }
        };

        template<class EH, class FUN, class... Args>
            auto sys_call(FUN* fun, Args&&... args)
            // compute return type of given function
            -> typename std::result_of<FUN&(Args...)>::type
            {
                // apply error check method
                return EH::check(fun(std::forward<Args>(args)...));
            }

        namespace eh { /* shortcuts for error_handlers */
                // TODO remove me
                using eeh = cxxux::sys::empty_error_handler;
                using seh = cxxux::sys::system_error_handler;

                using peh = cxxux::sys::positiv_errno_handler;
                using neh = cxxux::sys::negativ_errno_handler;

        } /* namespace eh */


    } /* namespace sys */
} /* namespace cxxux */

