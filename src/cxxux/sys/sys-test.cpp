/*
 * Copyright (c) 2014, 2015 Adam Szaj <adam.szaj [at] gmail [dot] com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 */



#include "signal.h"
#include "file.h"
#include "system.h"
#include "sys_call.h"
#include "descriptor.h"
#include "file_stat.h"
#include "time.h"

#include <iostream>
#include <fstream>
#include <cstring>
#include <set>
#include <thread>

#include <fcntl.h>
#include <unistd.h>
#include <execinfo.h>

using std::cout;
using std::ifstream;
using std::system_error;
using std::thread;

using namespace cxxux::sys;
using namespace cxxux::sys::eh;

std::string get_symbol(void* ptr)
{
    void* ptrs[] {ptr};
    char** symbols = backtrace_symbols(ptrs, 1);
    if (symbols) {
        std::string s{symbols[0]};
        free(symbols);
        return s;
    }
    return {};
}

namespace sys = cxxux::sys;
template<class Ch> std::vector<Ch> string_to_vector(const std::basic_string<Ch>& s)
{
    return std::vector<Ch>(s.begin(), s.end());
}
//
// --logger=syslog://
// --logger=file://<file_path>
// --logger=udp://<ip>:port
// --logger=tcp://<bind_ip>:port
// using namespace std;

const char* bin_name(const char* path) {
    const char* n = strrchr(path, '/');
    if (n) {
        return n+1;
    }
    return path;
}

void time_test() {
    using cxxux::sys::timespec;
    std::set<timespec> tss;
    std::vector<timespec> tsv;

    for (int i = 0; i < 10000; ++i){
        tsv.push_back(timespec::rt_now());
    }
    auto t0 = timespec::monotonic_now();
    for (auto it = tsv.crbegin(); it != tsv.crend(); ++it) {
        tss.insert(*it);
    }
    auto t1 = timespec::monotonic_now();

    /*
    timespec last;
    for (auto& ts: tss) {
        std::cout << ts << " " << (last < ts) << "\n";
        last = ts;
    }
    */

    auto t2 = timespec::monotonic_now();
    while (!tss.empty()) {
        tss.erase(tss.begin());
    }
    auto t3 = timespec::monotonic_now();


    std::cout << "insert: " << t0 - t1 << "\n";
    std::cout << "erase: " << t2 - t3 << "\n";
}
#if 0
void logger_test(int argc, char* argv[]) {
    set_logger(bin_name(argv[0]), "syslog://debug?option=LOG_CONS,LOG_PERROR&facility=LOG_USER");
    set_thread_name("main");

    thread th1([]{
            set_thread_name("th1");
            log_notice("hello world!!!\n");
            log_notice("hello world!!!\n");
            log_notice("hello world!!!\n");
            });
    log_notice("hello\na\nb\nc\nworld!!!\n");
    log_notice("hello world!!!\n");
    log_notice("hello world!!!\n");
    log_notice("hello world!!!\n");
    log_notice("hello world!!!\n");

    th1.join();
}
#endif

int main(int argc, const char *argv[]) try {
    time_test();
} catch (const std::exception& e) {
    std::cerr << "exc: " << e.what() << std::endl;
    return 1;
} catch (...) {
    std::cerr << "unknown exception" << std::endl;
    return 2;
}

