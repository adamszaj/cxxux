#include <gtest/gtest.h>

static int cube(int a) {
    return a*a*a;
}

TEST(testMath, myCubeTest)
{
    EXPECT_EQ(1000, cube(10));
}
