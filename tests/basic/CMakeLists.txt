SET(GTEST_LIBS gtest_main gcov)

ADD_EXECUTABLE(test_basic test_basic.cpp)
TARGET_LINK_LIBRARIES(test_basic ${GTEST_LIBS})