#include <gtest/gtest.h>
#include <stdlib.h>
#include <cxxux/sys/descriptor.h>
#include <cxxux/sys/file.h>

using namespace cxxux::sys;

static int make_temp(std::string& name, int flags) {
    std::vector<char> name_pattern;
    name_pattern.reserve(name.size()+1);
    name_pattern.insert(name_pattern.begin(), name.begin(), name.end());
    int desc = sys_call<system_error_handler>(::mkostemp, name_pattern.data(), flags);
    name = name_pattern.data();
    return desc;
}

TEST(unique_descriptor, ctor_test1)
{
    unique_descriptor desc{};
    EXPECT_EQ(-1, desc.get());
    EXPECT_EQ(-1, int(desc));
}

TEST(unique_descriptor, ctor_test2)
{
    unique_descriptor desc{1, unique_descriptor::dont_close_at_destroy_tag{}};
    EXPECT_EQ(1, desc.get());
    EXPECT_EQ(1, int(desc));
}

TEST(unique_descriptor, ctor_test3)
{
    std::string name{"/dev/shm/test_temp_XXXXXX"};
    int fd = -1;
    {
        unique_descriptor desc{make_temp(name, O_RDWR | O_CREAT | O_TRUNC)};
        EXPECT_NE(-1, desc.get());
        fd = desc.get();
        EXPECT_EQ(name.size() , cxxux::sys::write(fd, name.c_str(), name.size()));
    }
    unlink(name);
}

TEST(unique_descriptor, ctor_test4)
{
    std::string name{"/dev/shm/test_temp_XXXXXX"};
    {
        unique_descriptor desc{make_temp(name, O_RDWR | O_CREAT | O_TRUNC), unique_descriptor::close_at_destroy_tag{}};
        EXPECT_NE(-1, desc.get());
    }
    unlink(name);
}

TEST(unique_descriptor, release_test1)
{
    unique_descriptor desc{1, unique_descriptor::dont_close_at_destroy_tag{}};
    EXPECT_EQ(1, desc.release());
    EXPECT_EQ(-1, desc.release());
}

TEST(unique_descriptor, close_test1)
{
    std::string name{"/dev/shm/test_temp_XXXXXX"};
    unique_descriptor desc{make_temp(name, O_RDWR | O_CREAT | O_TRUNC)};
    EXPECT_NE(-1, desc.get());
    desc.close();
    EXPECT_EQ(-1, desc.get());
    unlink(name);
}

TEST(unique_descriptor, fcntl_test1)
{
    unique_descriptor desc{0, unique_descriptor::dont_close_at_destroy_tag{}};

    int flags{};
    EXPECT_NO_THROW(flags = desc.fcntl(F_GETFD, 0));
    EXPECT_NO_THROW(desc.fcntl(F_SETFD, flags));
}

TEST(unique_descriptor, ioctl_test1)
{
    unique_descriptor desc{0, unique_descriptor::dont_close_at_destroy_tag{}};
    EXPECT_ANY_THROW(desc.ioctl(0, 0));
}

TEST(unique_descriptor, operator_assign_test1)
{
    unique_descriptor desc_1{1, unique_descriptor::dont_close_at_destroy_tag{}};
    unique_descriptor desc_2{2, unique_descriptor::dont_close_at_destroy_tag{}};
    EXPECT_EQ(1, desc_1.get());
    EXPECT_EQ(2, desc_2.get());

    desc_1 = std::move(desc_2);

    EXPECT_EQ(2, desc_1.get());
    EXPECT_EQ(-1, desc_2.get());
}

TEST(unique_descriptor, operator_assign_test2)
{
    unique_descriptor desc_1{1, unique_descriptor::dont_close_at_destroy_tag{}};
    unique_descriptor desc_2{2, unique_descriptor::dont_close_at_destroy_tag{}};

    EXPECT_EQ(1, desc_1.get());
    EXPECT_EQ(2, desc_2.get());

    desc_1 = std::move(desc_1);

    EXPECT_EQ(1, desc_1.get());
}
