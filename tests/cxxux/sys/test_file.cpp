#include <gtest/gtest.h>
#include <stdlib.h>
#include <cxxux/sys/descriptor.h>
#include <cxxux/sys/file.h>

using namespace cxxux::sys;

TEST(file, compilation_test) {

    unique_file file;

    using eh = cxxux::sys::empty_error_handler;

    char buff[128];
    struct iovec iov;
    std::vector<iovec> iovs(4);
    std::vector<char>  cv(40);

    file.read(buff, sizeof(buff));
    file.write(buff, sizeof(buff));

    file.aread(buff, sizeof(buff));
    file.awrite(buff, sizeof(buff));

    file.readv(&iov, 1);
    file.writev(&iov, 1);

    file.readv(iovs);
    file.readv(iovs,0);
    file.readv(iovs,0,4);

    file.writev(iovs);
    file.writev(iovs,0);
    file.writev(iovs,0,4);

    file.read(cv);
    file.read(cv, 0);
    file.read(cv, 0, cv.size());

    file.write(cv);
    file.write(cv, 0);
    file.write(cv, 0, cv.size());

    file.read_eh<eh>(buff, sizeof(buff));
    file.write_eh<eh>(buff, sizeof(buff));
    file.readv_eh<eh>(&iov, 1);
    file.writev_eh<eh>(&iov, 1);

    file.ioctl(0, nullptr);
    file.fcntl(0, nullptr);

    file.fd();
    file.cfd();
    file.close();

}
