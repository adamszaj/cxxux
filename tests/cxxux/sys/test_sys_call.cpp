#include <gtest/gtest.h>
#include <stdlib.h>
#include <cerrno>
#include <cxxux/sys/sys_call.h>

using namespace cxxux::sys;

static int test_function(int v, int e) {
    errno = e;
    return v;
}

TEST(sys_call, sys_call)
{
    EXPECT_EQ( 0, sys_call<empty_error_handler>(test_function,  0, 0));
    EXPECT_EQ( 1, sys_call<empty_error_handler>(test_function,  1, 0));
    EXPECT_EQ(-1, sys_call<empty_error_handler>(test_function, -1, 0));
}

TEST(sys_call, system_error_test)
{
    EXPECT_EQ( 0, sys_call<system_error_handler>(test_function,  0, 0));
    EXPECT_EQ( 1, sys_call<system_error_handler>(test_function,  1, 0));
    EXPECT_THROW(sys_call<system_error_handler>(test_function, -1, EPERM), std::system_error);
}

TEST(sys_call, asystem_error_test)
{
    EXPECT_EQ( -1, sys_call<asystem_error_handler>(test_function,  0, 0));
    EXPECT_EQ( 1, sys_call<asystem_error_handler>(test_function,  1, 0));

    EXPECT_EQ( 0, sys_call<asystem_error_handler>(test_function, -1, EAGAIN));
    EXPECT_EQ( 0, sys_call<asystem_error_handler>(test_function, -1, EWOULDBLOCK));
    EXPECT_THROW( sys_call<asystem_error_handler>(test_function, -1, EPERM), std::system_error);
}

TEST(sys_call, positiv_errno_test)
{
    EXPECT_EQ( 0, sys_call<positiv_errno_handler>(test_function,  0, 0));
    EXPECT_EQ( -1, sys_call<positiv_errno_handler>(test_function,  -1, 0));
    EXPECT_THROW(sys_call<positiv_errno_handler>(test_function, EPERM, 0), std::system_error);
    EXPECT_THROW(sys_call<positiv_errno_handler>(test_function, EBADF, 0), std::system_error);
}

TEST(sys_call, negativ_errno_test)
{
    EXPECT_EQ( 0, sys_call<negativ_errno_handler>(test_function,  0, 0));
    EXPECT_EQ( 1, sys_call<negativ_errno_handler>(test_function,  1, 0));
    EXPECT_THROW(sys_call<negativ_errno_handler>(test_function, -EPERM, 0), std::system_error);
    EXPECT_THROW(sys_call<negativ_errno_handler>(test_function, -EBADF, 0), std::system_error);
}



