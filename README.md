# README #

### How to start ###
```
git clone <url>
cd cxxux

# if you want tests
./bootstrap
# endif

mkdir build
cd build
# if you want tests
cmake -G'Unix Makefiles' -DCXXUX_ENABLE_TESTS=ON ..
# else
cmake -G'Unix Makefiles' ..
# endif
```