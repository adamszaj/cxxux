#include <cxxux/net/socket.h>
#include <cxxux/sys/signal.h>
#include <cxxux/sys/time.h>
#include <cxxux/sys/poll.h>
#include <cxxux/utils/std.h>
#include <set>
#include <stack>


using namespace cxxux::net;
using namespace cxxux::sys;

struct service_listener {
    virtual ~service_listener() {}
    virtual void service_closed(size_t id) = 0;
};

using spoll = cxxux::sys::poll;

struct echo_service : public spoll::entry {

    public:
        echo_service(echo_service&& es) = default;

        echo_service(size_t id, service_listener* slst, unique_descriptor&& ud)
            : id {id}, slst(slst), sock {std::move(ud)}, buff(256), off {}, n {}, init_pollflag {POLLIN}
        {
            // sock.fd().fcntl(F_SETFL, O_NONBLOCK);
        }

        ~echo_service()
        {
        }

    protected:
        short events(short revents) noexcept {
            short ret = 0;

            switch (revents) {
                case -1:
                    setup_timeout();
                    ret = init_pollflag;
                    break;

                case 0:
                    ret = -1;
                    break;

                default:
                    ret = do_job(revents);

                    if (ret < 0) {
                        clear_timeout();
                    } else {
                        reset_timeout();
                    }

                    break;
            }

            if ((ret < 0) && slst) {
                slst->service_closed(id);
            }

            return ret;
        }

        int fd() const noexcept {
            return sock.fd().get();
        }

    private:

        size_t id;
        service_listener* slst;

        stream_socket<unique_descriptor> sock;
        std::vector<char> buff;
        size_t off;
        size_t n;
        short init_pollflag;

        short do_job(short revents) noexcept {
            try {
                short new_events = init_pollflag;
                if (revents & POLLOUT) {
                    // TODO change to asend
                    auto s = sock.send(buff, off, n, socket_ioflag{MSG_NOSIGNAL});

                    if (s == static_cast<ssize_t>(n)) {
                        off = n = 0;

                        if (new_events == 0) {
                            std::cout << "no more data to write\n";
                            return -1;
                        }
                    } else if (s < static_cast<ssize_t>(n)) {
                        n -= s;
                        off += s;
                    }

                    if (n > 0) {
                        if ((off + n) == buff.size()) {
                            // if buff is full accept only OUT events
                            new_events = POLLOUT;
                        } else {
                            // if buff is not full accept (OUT | IN) events
                            new_events |= POLLOUT;
                        }
                    }
                }

                if (revents & (POLLIN | POLLERR)) {
                    auto s = sock.recv(buff, {off + n}, socket_ioflag{MSG_NOSIGNAL});

                    if (s == 0) {
                        new_events = init_pollflag = 0;

                        if (n == 0) {
                            std::cout << "client closed write end\n";
                            return -1;
                        }
                    }

                    if (s > 0) {
                        n += s;
                    }

                    // if there are data in buffer accept OUT events
                    if (n > 0) {
                        new_events |= POLLOUT;
                    }
                }
                return new_events;
            } catch (const std::exception& e) {
                std::cout << "exception: '" << e.what() << "' closing connection\n";
            }
            return -1;
        }

        void reset_timeout() {
            clear_timeout();
            setup_timeout();
        }

        void setup_timeout() {
            auto now = cxxux::sys::timespec::monotonic_now();
            // 5 sec timeout
            now.value.tv_sec += 5;
            spoll::entry::set_timeout(now);
        }


};

class echo_server :
    public service_listener,
    public cxxux::sys::poll::entry {
        public:

            echo_server(std::string bind_addr)
                : ssock {{std::move(bind_addr), 7}, 10} {
                }

            echo_server(const echo_server& e) = delete;
            echo_server(echo_server&& e) = default;

            short events(short revents) noexcept {
                try {
                    if (revents > 0) {
                        if (revents & POLLIN) {
                            socket_address sa;
                            add_service(ssock.accept(sa));
                            std::cout << "got connection from client: " << sa << "\n";
                        }
                    }

                    return POLLIN;
                } catch (const std::exception& e) {
                    std::cout << "exc: " << e.what() << "\n";
                }
                return -1;
            }


            int fd() const noexcept {
                return ssock.fd().get();
            }

            void service_closed(size_t id) {
                services.at(id).reset();
                free_idxes.push(id);
            }

        private:
            server_socket ssock;

            std::vector<std::unique_ptr<echo_service>> services;
            std::stack<size_t>  free_idxes;

            size_t new_index() {
                size_t i {services.size()};

                if (!free_idxes.empty()) {
                    i = free_idxes.top();
                    free_idxes.pop();
                }

                return i;
            }

            void add_service(unique_descriptor&& desc) {
                auto i = new_index();
                auto ptr = std::make_unique<echo_service>(i, this, std::move(desc));
                auto raw_ptr = ptr.get();

                if (i == services.size()) {
                    services.push_back(std::move(ptr));
                } else {
                    services[i] = std::move(ptr);
                }

                get_poll()->add_entry(*raw_ptr);
            }
    };

using cxxux_poll = cxxux::sys::poll;

static bool g_running = true;

class quit_signal_handler : public cxxux::sys::poll_signal_handler {
    cxxux_poll& p_;
    bool quit_{false};
    public:
        quit_signal_handler(cxxux_poll& p, bool quit = true)
            : p_(p), quit_{quit} {}

        void signal(int signo) noexcept {
            std::cout << "got signal: " << strsignal(signo) << "\n";
            if (quit_)
                g_running = false;
            p_.quit();
        }
};


int main(int argc, char* argv[]) try {
    std::vector<std::string> args {argv, argv + argc};
    std::string default_bind {"0.0.0.0:7"};

    sys::signal_set process_mask{SIGINT, SIGQUIT, SIGTERM, SIGHUP, SIGPIPE};
    sys::process_signal_mask sigblocker{SIG_BLOCK, process_mask};

    sys::signal_set quit_mask{SIGINT, SIGQUIT, SIGTERM};
    sys::signal_set reload_mask{SIGHUP};

    while (g_running) {
        echo_server server((args.size() > 1) ? args[1] : default_bind);

        cxxux_poll p;

        quit_signal_handler qsh{p};
        quit_signal_handler rsh{p, false};

        /** accept signals denoted in mask */
        p.add_signal_handler(quit_mask, qsh);

        /** accept SIGHUP */
        p.add_signal_handler(reload_mask, rsh);

        p.add_entry(server);

        p();
    }

    return 0;
} catch (const std::exception& e) {
    std::cout << "exception: " << e.what() << "\n";
    return 1;
} catch (...) {
    std::cout << "unknown exception\n";
    return 2;
}

