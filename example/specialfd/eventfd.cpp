#include <iostream>
#include <unistd.h>
#include <fcntl.h>
#include <sys/eventfd.h>
#include <cxxux/sys/signal.h>
#include <cxxux/sys/sys_call.h>
#include <cxxux/sys/descriptor.h>
#include <cxxux/sys/file.h>
#include <cstring>

namespace sys = cxxux::sys;

int main(int argc, char *argv[]) {

    sys::signal_set sset{SIGINT, SIGTERM, SIGQUIT};
    sys::process_signal_mask smask{SIG_BLOCK, sset};
    sys::unique_descriptor desc{sys::sys_call<sys::system_error_handler>(::eventfd, 0, EFD_NONBLOCK)};

    uint64_t cnt{15};
    sys::write(desc.get(), &cnt, sizeof(cnt));

    while (sys::read_eh<sys::asystem_error_handler>(desc.get(), &cnt, sizeof(cnt)) == sizeof(cnt)) {
        std::cout << "event: " << cnt << "\n";
    }

    return 0;
}
