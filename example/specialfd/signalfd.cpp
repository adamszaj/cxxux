#include <iostream>
#include <unistd.h>
#include <fcntl.h>
#include <sys/signalfd.h>
#include <cxxux/sys/signal.h>
#include <cxxux/sys/sys_call.h>
#include <cxxux/sys/descriptor.h>
#include <cxxux/sys/file.h>
#include <cstring>

namespace sys = cxxux::sys;

int main(int argc, char *argv[]) {

    sys::signal_set sset{SIGINT, SIGTERM, SIGQUIT};
    sys::process_signal_mask smask{SIG_BLOCK, sset};

    sys::unique_descriptor desc{sys::sys_call<sys::system_error_handler>(::signalfd, -1, sset.get(), 0)};
    signalfd_siginfo sinfo;

    while (sys::read(desc.get(), &sinfo, sizeof(sinfo)) == sizeof(sinfo)) {
        int signo = sinfo.ssi_signo;
        std::cout << "signal: " << signo << " -> " << strsignal(signo) << "\n";
        if (signo == SIGTERM)
            break;
    }

    return 0;
}
