#include <cxxux/net/addrinfo.h>

using namespace std;
using namespace cxxux::net;

template<class T>
std::vector<T> subvector(const std::vector<T>& v, size_t begin, size_t count) {
    if (begin >= v.size()) {
        begin = v.size();
        count = 0;
    } else {
        if (begin+count >= v.size()) {
            count = v.size()-begin;
        }
    }
    return {v.begin()+begin, v.begin()+begin+count};
}

template<class T>
std::vector<T> subvector(const std::vector<T>& v, size_t begin) {
    if (begin >= v.size()) {
        begin = v.size();
    }
    return subvector(v, begin, v.size() - begin);
}



int main(int argc, char *argv[]) try {
    std::vector<std::string> args{argv+1, argv+argc};
    for (const auto& name: args) {
        for (const auto& addr: resolv_address(name, PF_UNSPEC, SOCK_STREAM, AI_CANONNAME)) {
            cout << name << " -> " << addr << "\n";
        }
    }
    return 0;
} /* end main() */
catch (const std::exception& e) {
    std::cerr << "exception: " << e.what() << std::endl;
}
catch (...) {
    std::cerr << "unknown exception\n";
}

